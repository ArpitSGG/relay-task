# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.3.5](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.3.4...v0.3.5) (2021-05-03)


### Bug Fixes

* add per in customer restriction widget ([5f0d0bb](https://gitlab.com/appointy/waqt/ui/service2/commit/5f0d0bbf21c16153dcb1cfcb1039e83c99fbe303))

### [0.3.4](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.3.3...v0.3.4) (2021-04-30)


### Bug Fixes

* reschedule change on cancellation change ([39e0bc6](https://gitlab.com/appointy/waqt/ui/service2/commit/39e0bc6d44bacbee85cd460e85a0dd846c067e68))
* validations ([72e53a0](https://gitlab.com/appointy/waqt/ui/service2/commit/72e53a01880c27d24e0e46743ec5798ae9c7a00d))

### [0.3.3](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.3.2...v0.3.3) (2021-04-28)


### Features

* duration on change in update ([082b110](https://gitlab.com/appointy/waqt/ui/service2/commit/082b1103813079a8dce416c3d949c7c08b3d98a5))

### [0.3.2](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.3.1...v0.3.2) (2021-04-28)


### Features

* update policy widget without form ([104f0ac](https://gitlab.com/appointy/waqt/ui/service2/commit/104f0ac264ddaae421b15864aaa6d8b9541397ad))


### Bug Fixes

* update policy widget with form ([43b32af](https://gitlab.com/appointy/waqt/ui/service2/commit/43b32af260a572699df4cbcc83e91fa5ed6b4b6e))

### [0.3.1](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.333...v0.3.1) (2021-04-19)


### Bug Fixes

* rights in avatar ([306c223](https://gitlab.com/appointy/waqt/ui/service2/commit/306c2230e07f50c0d9df0c634e284aec54cc7325))

### [0.2.333](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.332...v0.2.333) (2021-04-13)


### Bug Fixes

* rights for employee ([87ee2a9](https://gitlab.com/appointy/waqt/ui/service2/commit/87ee2a9b556943ca3b3057085d7ebba99b0e6b91))

### [0.2.332](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.331...v0.2.332) (2021-04-10)

### [0.2.331](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.330...v0.2.331) (2021-04-02)


### Bug Fixes

* pass location id in services of employee ([14e4ac4](https://gitlab.com/appointy/waqt/ui/service2/commit/14e4ac4c9134300b21c204b789624fdfed1e93f1))

### [0.2.330](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.329...v0.2.330) (2021-04-02)


### Bug Fixes

* parametric url ([e84f772](https://gitlab.com/appointy/waqt/ui/service2/commit/e84f772fb27a6019c43419dbbbae5ad7d3a999d7))

### [0.2.329](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.328...v0.2.329) (2021-03-22)


### Features

* durations in service form widget ([1f78b70](https://gitlab.com/appointy/waqt/ui/service2/commit/1f78b702b445a75a321a55ad9bc09af23ac02dae))

### [0.2.328](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.327...v0.2.328) (2021-03-18)


### Features

* hided booking rules for mathnasium ([0aa853b](https://gitlab.com/appointy/waqt/ui/service2/commit/0aa853b3e4e1d9b4817485209c32d7953e94901d))

### [0.2.327](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.326...v0.2.327) (2021-03-17)


### Features

* locale component integration ([b269c6d](https://gitlab.com/appointy/waqt/ui/service2/commit/b269c6d1ed245033ddff16d00ce2d635fce1c7e9))

### [0.2.326](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.325...v0.2.326) (2021-03-13)


### Features

* remove payment and tax setting from mathnasium in service section ([f0e7c02](https://gitlab.com/appointy/waqt/ui/service2/commit/f0e7c029b4ecbe81dd0917b4cc81065ceeab4cbd))

### [0.2.325](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.324...v0.2.325) (2021-03-12)


### Bug Fixes

* changed customer settings to consumer restrictions ([91295ea](https://gitlab.com/appointy/waqt/ui/service2/commit/91295ea1805f3dc45a9e9bfac6a69e646a57ce28))
* schema ([6e53783](https://gitlab.com/appointy/waqt/ui/service2/commit/6e5378366bf37b6543ae89278c03300df6db8d66))

### [0.2.324](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.323...v0.2.324) (2021-03-11)


### Features

* remove payment and tax setting from mathnasium ([737b5a0](https://gitlab.com/appointy/waqt/ui/service2/commit/737b5a0eb13a68027d824cc423fba7bd058bc046))

### [0.2.323](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.322...v0.2.323) (2021-03-03)


### Features

* hide internation restrictions and customer booking restriction for mathnasium ([345c103](https://gitlab.com/appointy/waqt/ui/service2/commit/345c1031d20c6ab2ae102b845efc72ecf923c7fa))

### [0.2.322](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.321...v0.2.322) (2021-03-03)


### Features

* assign all service and staff functionality ([525d8b9](https://gitlab.com/appointy/waqt/ui/service2/commit/525d8b90f1d7ed187792ffdfdd18a798185dcafe))

### [0.2.321](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.320...v0.2.321) (2021-02-25)


### Bug Fixes

* service form title ([1e46e2e](https://gitlab.com/appointy/waqt/ui/service2/commit/1e46e2ecb7ad0ad9ffc5ad254a6a08be8202cdc2))

### [0.2.320](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.319...v0.2.320) (2021-02-25)


### Bug Fixes

* service location widget ([765682e](https://gitlab.com/appointy/waqt/ui/service2/commit/765682e8a6072502d70e2f0c3c92d1a511ef60c4))

### [0.2.319](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.318...v0.2.319) (2021-02-25)


### Bug Fixes

* message ([6fdd7a1](https://gitlab.com/appointy/waqt/ui/service2/commit/6fdd7a17a7eac7ab145a0fa788362f307fa9e4d5))

### [0.2.318](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.317...v0.2.318) (2021-02-24)


### Features

* mathnasium text ([d88d31a](https://gitlab.com/appointy/waqt/ui/service2/commit/d88d31ac7b1a610eb1fc39f4c2098a2e439635f7))

### [0.2.317](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.316...v0.2.317) (2021-02-22)


### Bug Fixes

* hided at parent location option ([a29a24b](https://gitlab.com/appointy/waqt/ui/service2/commit/a29a24bda35fe3c78d9a7a0fc0c9bd2ed4ac79c0))

### [0.2.316](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.315...v0.2.316) (2021-02-16)


### Bug Fixes

* search ([dbc1bbb](https://gitlab.com/appointy/waqt/ui/service2/commit/dbc1bbbeda64e58db8b95ded7958a4296da5367f))

### [0.2.315](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.314...v0.2.315) (2021-02-08)


### Bug Fixes

* refetch in employees of service page ([959151d](https://gitlab.com/appointy/waqt/ui/service2/commit/959151d0a6aab768fbb1d2eb39c0c7326c7ec291))

### [0.2.314](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.313...v0.2.314) (2021-02-06)


### Bug Fixes

* proceed without selection of employees ([c9cd7fa](https://gitlab.com/appointy/waqt/ui/service2/commit/c9cd7fa1a535ee22c47cda1601e832a4ab50d62f))
* service settings buffer validations ([f0a4d5c](https://gitlab.com/appointy/waqt/ui/service2/commit/f0a4d5ce5d02e00ecde129852c0bb2060c03f0e5))

### [0.2.313](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.312...v0.2.313) (2021-02-01)


### Bug Fixes

* allow with penalty hour issue ([37b44bc](https://gitlab.com/appointy/waqt/ui/service2/commit/37b44bcb1e129e6bc4740bc916a6e85114c7bc8d))

### [0.2.312](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.311...v0.2.312) (2021-02-01)


### Bug Fixes

* title and description limit ([af49169](https://gitlab.com/appointy/waqt/ui/service2/commit/af491698f8b67d680c469d4e4bc89b84deb3ad4c))

### [0.2.311](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.310...v0.2.311) (2021-01-30)

### [0.2.310](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.309...v0.2.310) (2021-01-30)

### [0.2.309](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.308...v0.2.309) (2021-01-30)


### Bug Fixes

* validation mssg ([77d29b7](https://gitlab.com/appointy/waqt/ui/service2/commit/77d29b737de7ec33298d788acb75ed9f1dce08ba))

### [0.2.308](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.307...v0.2.308) (2021-01-29)


### Features

* service price override check and condition ([9fead4c](https://gitlab.com/appointy/waqt/ui/service2/commit/9fead4cca426f1ffb1f9cc7985f3bfcc0b61749f))

### [0.2.307](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.306...v0.2.307) (2021-01-28)


### Bug Fixes

* hided import ([7383dd0](https://gitlab.com/appointy/waqt/ui/service2/commit/7383dd0d1c792d897bff6f2dc425b01ff2bfbe08))
* validations ([8121bf9](https://gitlab.com/appointy/waqt/ui/service2/commit/8121bf936924d3d1854a7309485d59216b4e28dc))

### [0.2.306](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.305...v0.2.306) (2021-01-28)


### Features

* no override option for more than one employees ([afd5cd1](https://gitlab.com/appointy/waqt/ui/service2/commit/afd5cd116c25f7c22e9a077a893259df8173a590))

### [0.2.305](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.304...v0.2.305) (2021-01-27)


### Features

* search boxes in linking ([270a869](https://gitlab.com/appointy/waqt/ui/service2/commit/270a869dd8ce44ea292c9e533394b27faf09c67e))

### [0.2.304](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.303...v0.2.304) (2021-01-27)


### Features

* employee step ux ([2786a34](https://gitlab.com/appointy/waqt/ui/service2/commit/2786a34b80978f49df248fc3e83ba4d993c04530))
* set price link ([bd02fc2](https://gitlab.com/appointy/waqt/ui/service2/commit/bd02fc28f82f56ae48cd87cafde0ff83dae10289))

### [0.2.303](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.302...v0.2.303) (2021-01-25)


### Features

* do not show capacity for mathnasium ([8519d9a](https://gitlab.com/appointy/waqt/ui/service2/commit/8519d9ab6393d0b456cdff06ad4fafdbf8555852))


### Bug Fixes

* build issue ([a152db0](https://gitlab.com/appointy/waqt/ui/service2/commit/a152db0762b454c26d82853f63b757b93bfe7b3d))

### [0.2.302](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.301...v0.2.302) (2021-01-23)


### Bug Fixes

* switch settings error ([e734dad](https://gitlab.com/appointy/waqt/ui/service2/commit/e734dad80c4b880785e4cadc44b45a9ec366829c))

### [0.2.301](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.300...v0.2.301) (2021-01-22)


### Bug Fixes

* switching error ([2ab4823](https://gitlab.com/appointy/waqt/ui/service2/commit/2ab4823f1f64489bd7dc5266e254cd08ef622133))

### [0.2.300](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.299...v0.2.300) (2021-01-22)


### Features

* hide capacity for team teaching ([958faa6](https://gitlab.com/appointy/waqt/ui/service2/commit/958faa6e5decbe2b2599e97eaa37b9520dbd2f4d))

### [0.2.299](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.298...v0.2.299) (2021-01-22)


### Features

* hide staff restrictions for team teaching ([7232d4c](https://gitlab.com/appointy/waqt/ui/service2/commit/7232d4cd14587bedd6049b6f44e82e2c09f10061))

### [0.2.298](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.297...v0.2.298) (2021-01-22)


### Features

* hover checkbox ([cdab94b](https://gitlab.com/appointy/waqt/ui/service2/commit/cdab94b18fd40e0a1073ea35c21c4d07434ebf56))

### [0.2.297](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.296...v0.2.297) (2021-01-19)


### Bug Fixes

* linking height ([eb7a9b5](https://gitlab.com/appointy/waqt/ui/service2/commit/eb7a9b51237a58affe46add27352053eb02d74fc))

### [0.2.296](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.295...v0.2.296) (2021-01-19)


### Features

* payment gateway settings shortcut ([c7322ca](https://gitlab.com/appointy/waqt/ui/service2/commit/c7322ca43649aeab324b656c5efccf27d05eee9e))

### [0.2.295](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.294...v0.2.295) (2021-01-15)


### Bug Fixes

* booking rules ([b641835](https://gitlab.com/appointy/waqt/ui/service2/commit/b6418356c535d35b888b364a4cbaf2584666572b))

### [0.2.294](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.293...v0.2.294) (2021-01-15)


### Bug Fixes

* uniq rules ([2a1c986](https://gitlab.com/appointy/waqt/ui/service2/commit/2a1c9861c3b9d04f46ecda72d5214fcfb361aecb))

### [0.2.293](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.292...v0.2.293) (2021-01-15)


### Bug Fixes

* service table width ([0cf6461](https://gitlab.com/appointy/waqt/ui/service2/commit/0cf64613d0a53c4c55ea683c33b47d3310391451))

### [0.2.292](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.291...v0.2.292) (2021-01-15)


### Bug Fixes

* linking pagination ([005875c](https://gitlab.com/appointy/waqt/ui/service2/commit/005875c98476485acd0634bbf8f145507aede46c))
* service access text ([59226a9](https://gitlab.com/appointy/waqt/ui/service2/commit/59226a969c4b6f68e6a0dbcc6b8d317597ecc49a))

### [0.2.291](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.290...v0.2.291) (2021-01-14)


### Bug Fixes

* booking rules switching error ([9ea0b97](https://gitlab.com/appointy/waqt/ui/service2/commit/9ea0b97d88566671f6e1cbb3a5452b98cabf79d2))

### [0.2.290](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.289...v0.2.290) (2021-01-12)

### [0.2.289](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.288...v0.2.289) (2021-01-12)


### Bug Fixes

* update pricing if only duration is updated ([3894c90](https://gitlab.com/appointy/waqt/ui/service2/commit/3894c90dc14c5fa89fccd27c5dce8a1135a8714d))

### [0.2.288](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.287...v0.2.288) (2021-01-12)


### Bug Fixes

* remove business hours step from add ([df338ad](https://gitlab.com/appointy/waqt/ui/service2/commit/df338ad97b02cbfb7f16808f07d6a436b414cec1))

### [0.2.287](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.286...v0.2.287) (2021-01-09)


### Bug Fixes

* booking rules customer restriction datepicker ([64daf3b](https://gitlab.com/appointy/waqt/ui/service2/commit/64daf3bbcdfb2b4a387a6a968846fd354b5c46db))

### [0.2.286](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.285...v0.2.286) (2021-01-08)


### Bug Fixes

* alias employees ([8f87b6e](https://gitlab.com/appointy/waqt/ui/service2/commit/8f87b6e244abac4b8aa807d4d6bdc06d95a24cd8))

### [0.2.285](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.284...v0.2.285) (2021-01-07)

### [0.2.284](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.283...v0.2.284) (2021-01-06)

### [0.2.283](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.282...v0.2.283) (2021-01-05)


### Bug Fixes

* alias in buffer widget ([ba09818](https://gitlab.com/appointy/waqt/ui/service2/commit/ba098185879114640320c7de7687f3cda9221912))

### [0.2.282](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.281...v0.2.282) (2021-01-05)


### Bug Fixes

* at studio alias ([1c8c509](https://gitlab.com/appointy/waqt/ui/service2/commit/1c8c50909396de3f5c9635dbf184fe0df439f413))
* min value ([a954632](https://gitlab.com/appointy/waqt/ui/service2/commit/a954632ffd8239d2a9661e2efc8a7775ad85090c))
* service add step form validation ([cfc53a3](https://gitlab.com/appointy/waqt/ui/service2/commit/cfc53a32916a02e8c853210ea3ae864c6836c8ea))

### [0.2.281](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.280...v0.2.281) (2021-01-04)


### Bug Fixes

* customer settings ([caff5dc](https://gitlab.com/appointy/waqt/ui/service2/commit/caff5dcf5779a042c6b69d35f8ee7d6408784e6e))

### [0.2.280](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.279...v0.2.280) (2021-01-02)


### Bug Fixes

* aliasing in second step ([4e6ec18](https://gitlab.com/appointy/waqt/ui/service2/commit/4e6ec18be5d66e6848037646c8bf0c3d1a4d0b7a))

### [0.2.279](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.278...v0.2.279) (2021-01-02)


### Bug Fixes

* aliasing in service type map ([332e1e0](https://gitlab.com/appointy/waqt/ui/service2/commit/332e1e058da59bc4a5b6c93c1894df2098fe55e0))

### [0.2.278](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.277...v0.2.278) (2021-01-02)


### Bug Fixes

* aliasing in service type map ([a4280d0](https://gitlab.com/appointy/waqt/ui/service2/commit/a4280d00e75f7a8e936b4a403a7b6f9b188984a3))

### [0.2.277](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.276...v0.2.277) (2021-01-02)


### Bug Fixes

* aliasing ([3fda445](https://gitlab.com/appointy/waqt/ui/service2/commit/3fda445c0f5174e1ca44d290a717515aebc75687))

### [0.2.276](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.275...v0.2.276) (2020-12-30)

### [0.2.275](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.274...v0.2.275) (2020-12-29)


### Bug Fixes

* alias in messages.tsx ([74d0d48](https://gitlab.com/appointy/waqt/ui/service2/commit/74d0d48e7a166fd6e1a7732b9c5ba86fc165752e))

### [0.2.274](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.273...v0.2.274) (2020-12-29)


### Bug Fixes

* alias in messages.tsx ([a3104d6](https://gitlab.com/appointy/waqt/ui/service2/commit/a3104d6e1177da669733f52ef2c0e1f4aa078f77))

### [0.2.273](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.272...v0.2.273) (2020-12-29)


### Bug Fixes

* release issue ([dfd8325](https://gitlab.com/appointy/waqt/ui/service2/commit/dfd83254776905671aa1882a837502ed22816c1e))

### [0.2.272](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.271...v0.2.272) (2020-12-29)

### [0.2.271](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.270...v0.2.271) (2020-12-28)


### Bug Fixes

* intake forms ([d5c7af9](https://gitlab.com/appointy/waqt/ui/service2/commit/d5c7af97362dc6f8558b09e9f3c1764ad40ba0a3))

### [0.2.270](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.269...v0.2.270) (2020-12-28)

### [0.2.269](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.268...v0.2.269) (2020-12-28)

### [0.2.268](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.267...v0.2.268) (2020-12-25)


### Features

* inactive employees and all employees assigned case ([96b7043](https://gitlab.com/appointy/waqt/ui/service2/commit/96b704379644e3ef4d8d0b58cea11c9b0b90f600))
* inactive services and all services assigned case ([acc3bb7](https://gitlab.com/appointy/waqt/ui/service2/commit/acc3bb71306321fa1b2bc2248e293bc38b11d852))


### Bug Fixes

* employees of service scroll ([3edeb66](https://gitlab.com/appointy/waqt/ui/service2/commit/3edeb66002086b51d749f3a697116d0784f48d0e))

### [0.2.267](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.266...v0.2.267) (2020-12-24)


### Bug Fixes

* remove log ([b983a42](https://gitlab.com/appointy/waqt/ui/service2/commit/b983a424f996a9c92e3c64ce797257075513a939))

### [0.2.266](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.265...v0.2.266) (2020-12-23)


### Bug Fixes

* refetch service on change ([3b4897d](https://gitlab.com/appointy/waqt/ui/service2/commit/3b4897d047ca543954ef95e73a68f8ac921dc80a))

### [0.2.265](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.264...v0.2.265) (2020-12-23)


### Bug Fixes

* build issues ([b565517](https://gitlab.com/appointy/waqt/ui/service2/commit/b5655176c9469dcc212f41f90412772e59b3f527))

### [0.2.264](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.263...v0.2.264) (2020-12-23)

### [0.2.263](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.262...v0.2.263) (2020-12-22)


### Bug Fixes

* show categories sortable if more than one ([a9d7bb7](https://gitlab.com/appointy/waqt/ui/service2/commit/a9d7bb7bf9ad67205cfec84fb60a8e095956274c))

### [0.2.262](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.261...v0.2.262) (2020-12-21)


### Bug Fixes

* discount hook ([6a525e2](https://gitlab.com/appointy/waqt/ui/service2/commit/6a525e2e493cedb476d59ec003959983d85b0d95))

### [0.2.261](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.260...v0.2.261) (2020-12-21)


### Bug Fixes

* service type add ([005333a](https://gitlab.com/appointy/waqt/ui/service2/commit/005333ade6ea76cc4117b3f82286e9f719eb88a4))

### [0.2.260](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.259...v0.2.260) (2020-12-20)


### Bug Fixes

* discount hook ([05a0315](https://gitlab.com/appointy/waqt/ui/service2/commit/05a031571239768123c31200d17d6f3ff990c57c))

### [0.2.259](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.258...v0.2.259) (2020-12-20)


### Bug Fixes

* filter services based on locations ([2ef48a9](https://gitlab.com/appointy/waqt/ui/service2/commit/2ef48a9fa7827109e307f0857dd75d01c3457246))

### [0.2.258](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.257...v0.2.258) (2020-12-20)


### Bug Fixes

* service form widget select all and grouping ([b14dcac](https://gitlab.com/appointy/waqt/ui/service2/commit/b14dcaceeb7e4f8cac981934d0f580d04fcac11d))

### [0.2.257](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.256...v0.2.257) (2020-12-20)


### Features

* list employees not performing service ([ee71268](https://gitlab.com/appointy/waqt/ui/service2/commit/ee71268cf54ef4d614c44584a59dd8465e6a928a))
* list unlinked services of employee ([d6706e7](https://gitlab.com/appointy/waqt/ui/service2/commit/d6706e72e0164916fdf323015c166f0fd223c4e0))

### [0.2.256](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.255...v0.2.256) (2020-12-19)


### Bug Fixes

* empty state ([c8f8462](https://gitlab.com/appointy/waqt/ui/service2/commit/c8f84628718e35d9c84dd89bfbea17cab793f3a3))

### [0.2.255](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.254...v0.2.255) (2020-12-18)


### Bug Fixes

* discount hook ([9433239](https://gitlab.com/appointy/waqt/ui/service2/commit/94332392a22fca0aa98e5ea251efc5c32d97b9f8))

### [0.2.254](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.253...v0.2.254) (2020-12-18)


### Features

* discount hook ([66a94db](https://gitlab.com/appointy/waqt/ui/service2/commit/66a94db0a19a288732dc3b67b7e8670ec02f83cc))

### [0.2.253](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.252...v0.2.253) (2020-12-16)


### Features

* filter services ([7d7bc5e](https://gitlab.com/appointy/waqt/ui/service2/commit/7d7bc5efa0691edeaebaabbf360d9fe27a59e192))

### [0.2.252](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.251...v0.2.252) (2020-12-16)


### Features

* selected services ([e9b1ae0](https://gitlab.com/appointy/waqt/ui/service2/commit/e9b1ae061bf2bf1e316663f295023d79f69bfdc3))

### [0.2.251](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.250...v0.2.251) (2020-12-16)

### [0.2.250](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.249...v0.2.250) (2020-12-16)


### Bug Fixes

* price null ([a1632d3](https://gitlab.com/appointy/waqt/ui/service2/commit/a1632d3143d31ba4a5e5adafebbc51473b4e50b5))

### [0.2.249](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.248...v0.2.249) (2020-12-16)


### Features

* prevent grouping ([df02b48](https://gitlab.com/appointy/waqt/ui/service2/commit/df02b48df6337fd5cd12b88b928e4416c86faf3a))

### [0.2.248](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.247...v0.2.248) (2020-12-15)

### [0.2.247](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.246...v0.2.247) (2020-12-15)


### Bug Fixes

* handle override in grouping ([dce34e0](https://gitlab.com/appointy/waqt/ui/service2/commit/dce34e03fc2c9b3eb57ed753ddd5184be1771ead))

### [0.2.246](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.245...v0.2.246) (2020-12-15)


### Features

* already selected values ([ff98046](https://gitlab.com/appointy/waqt/ui/service2/commit/ff9804607e24f7c01cf070e2c23a01bf94948fcb))
* handle selected ([bccfc81](https://gitlab.com/appointy/waqt/ui/service2/commit/bccfc81493ba88e4245b789fec9107db6f59dd28))

### [0.2.245](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.244...v0.2.245) (2020-12-15)


### Bug Fixes

* performance ([2e083e9](https://gitlab.com/appointy/waqt/ui/service2/commit/2e083e9e7a82260f79c9e11b6cc8d39ef92e1a94))

### [0.2.244](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.243...v0.2.244) (2020-12-14)


### Features

* show select all ([9f566f4](https://gitlab.com/appointy/waqt/ui/service2/commit/9f566f439e0d046e106a3e64b4cfbc9fb897a344))


### Bug Fixes

* height ([11c464e](https://gitlab.com/appointy/waqt/ui/service2/commit/11c464efbed4840e0fbb0cb98226d7d5f77b76ae))

### [0.2.243](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.242...v0.2.243) (2020-12-14)


### Features

* service list ([13b5e0a](https://gitlab.com/appointy/waqt/ui/service2/commit/13b5e0af0449d32f6c3d8a43a35b4e88add16961))

### [0.2.242](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.241...v0.2.242) (2020-12-14)


### Features

* service list ([1c11c9f](https://gitlab.com/appointy/waqt/ui/service2/commit/1c11c9fb5837bbcec16b9521687822368b71bd1a))

### [0.2.241](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.240...v0.2.241) (2020-12-14)


### Features

* smart widget ([bbd76d0](https://gitlab.com/appointy/waqt/ui/service2/commit/bbd76d04e06dcba6c0dbd2ade7ed12eb2201874b))
* smart widget ([ace30b9](https://gitlab.com/appointy/waqt/ui/service2/commit/ace30b9be5b4d8e06b21c1d916115147229be991))
* smart widget ([33ba60a](https://gitlab.com/appointy/waqt/ui/service2/commit/33ba60a2cf9d3b9c664acb9071d5ab4e352a3807))

### [0.2.240](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.239...v0.2.240) (2020-12-14)


### Features

* smart widget take one ([3266d3b](https://gitlab.com/appointy/waqt/ui/service2/commit/3266d3b67e991598815c3813ffa2f9232e0614f7))

### [0.2.239](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.237...v0.2.239) (2020-12-09)


### Bug Fixes

* sortable js missing ([0839b61](https://gitlab.com/appointy/waqt/ui/service2/commit/0839b614c69b0edcfc315240e198995e051df086))
* sortable js missing ([b7932b7](https://gitlab.com/appointy/waqt/ui/service2/commit/b7932b7dc8fc3bd753bf589acf471c3a7e44c763))

### [0.2.238](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.237...v0.2.238) (2020-12-09)


### Bug Fixes

* sortable js missing ([0839b61](https://gitlab.com/appointy/waqt/ui/service2/commit/0839b614c69b0edcfc315240e198995e051df086))
* sortable js missing ([b7932b7](https://gitlab.com/appointy/waqt/ui/service2/commit/b7932b7dc8fc3bd753bf589acf471c3a7e44c763))

### [0.2.237](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.236...v0.2.237) (2020-12-09)


### Features

* intake form integration ([38cd1f1](https://gitlab.com/appointy/waqt/ui/service2/commit/38cd1f1f8ddf0aedc93844ffcc6173dab14ca496))


### Bug Fixes

* removed console logs ([68f87e5](https://gitlab.com/appointy/waqt/ui/service2/commit/68f87e557931936d23e6e0efa875c65c56df10bf))

### [0.2.236](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.235...v0.2.236) (2020-12-07)

### [0.2.235](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.234...v0.2.235) (2020-12-07)


### Bug Fixes

* sortable version ([fd23605](https://gitlab.com/appointy/waqt/ui/service2/commit/fd23605af5f5984e48bda8720f8069b6c21c75c8))

### [0.2.234](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.233...v0.2.234) (2020-11-27)


### Bug Fixes

* country codes ([a648901](https://gitlab.com/appointy/waqt/ui/service2/commit/a64890165afc21a5d6d226a16ed45127971792ef))

### [0.2.233](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.232...v0.2.233) (2020-11-16)


### Bug Fixes

* do not allow negative values in buffer time settings ([f77e53b](https://gitlab.com/appointy/waqt/ui/service2/commit/f77e53b85a8ace769d4c3691c1a5d43f9570ed65))

### [0.2.232](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.231...v0.2.232) (2020-11-13)


### Bug Fixes

* interval widget ([df7de21](https://gitlab.com/appointy/waqt/ui/service2/commit/df7de2171e05b8cebc40f6458d3d00a8ec5fa3dd))
* rights ([bcafc69](https://gitlab.com/appointy/waqt/ui/service2/commit/bcafc6927da9316a7430a08772edfa8a799f0ce2))
* settings ([f40384f](https://gitlab.com/appointy/waqt/ui/service2/commit/f40384fbf2a65252a0aa488aa37348b4675d4fac))

### [0.2.231](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.230...v0.2.231) (2020-11-12)


### Bug Fixes

* linking rights ([ff53dd0](https://gitlab.com/appointy/waqt/ui/service2/commit/ff53dd025ea01321eb909bafe79c6c2e1475e8fd))

### [0.2.230](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.229...v0.2.230) (2020-11-11)


### Bug Fixes

* services ([703ab12](https://gitlab.com/appointy/waqt/ui/service2/commit/703ab121377e02e8ee4941dd017cb84e11430f4c))

### [0.2.229](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.228...v0.2.229) (2020-11-11)


### Bug Fixes

* location name ([a290934](https://gitlab.com/appointy/waqt/ui/service2/commit/a290934e0619880b4d2a6bad3d38932394245ec2))

### [0.2.228](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.227...v0.2.228) (2020-11-11)


### Bug Fixes

* location name ([7425307](https://gitlab.com/appointy/waqt/ui/service2/commit/742530796fa3a1c7022d4a780bb62f8b2df2f825))

### [0.2.227](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.226...v0.2.227) (2020-11-10)


### Bug Fixes

* payment settings widget validation ([a5a0ae7](https://gitlab.com/appointy/waqt/ui/service2/commit/a5a0ae71e63ad2fa2dea407c825e48091d1be9a3))

### [0.2.226](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.225...v0.2.226) (2020-11-10)


### Bug Fixes

* validations ([cde5b2c](https://gitlab.com/appointy/waqt/ui/service2/commit/cde5b2cf1e7e39aee3e9a431f682dda8b3e17e4a))

### [0.2.225](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.224...v0.2.225) (2020-11-09)


### Bug Fixes

* rights in linking ([e22f089](https://gitlab.com/appointy/waqt/ui/service2/commit/e22f089178ff3e25daa59ba5f9ffeeb981e59ec7))

### [0.2.224](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.223...v0.2.224) (2020-11-09)


### Bug Fixes

* rights in actions ([47fecd4](https://gitlab.com/appointy/waqt/ui/service2/commit/47fecd41a8a9323ae0432a3beed864b46aa4a132))

### [0.2.223](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.222...v0.2.223) (2020-11-09)


### Bug Fixes

* rights in actions ([ba6debe](https://gitlab.com/appointy/waqt/ui/service2/commit/ba6debed3b32da4dfceade91c75ab9a68f3a18cc))

### [0.2.222](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.221...v0.2.222) (2020-11-09)


### Bug Fixes

* services viewer rights ([8dc6811](https://gitlab.com/appointy/waqt/ui/service2/commit/8dc68112f9146275404b5ec104300dff0fc1e592))

### [0.2.221](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.220...v0.2.221) (2020-11-07)


### Bug Fixes

* remove search ([8601e8c](https://gitlab.com/appointy/waqt/ui/service2/commit/8601e8c3f48bb8749474a8016caf6aa0bb03d7b4))

### [0.2.220](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.219...v0.2.220) (2020-11-07)


### Bug Fixes

* customer setting action ([a527049](https://gitlab.com/appointy/waqt/ui/service2/commit/a5270491b56924d83948f2edb670246836d95663))

### [0.2.219](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.218...v0.2.219) (2020-11-07)


### Features

* customer bulk action take one ([f8ebdc7](https://gitlab.com/appointy/waqt/ui/service2/commit/f8ebdc7ecfedc53af6bd5e0ba2ccbaf2b7cc8fa2))


### Bug Fixes

* customer widget ux ([f2957ef](https://gitlab.com/appointy/waqt/ui/service2/commit/f2957ef5147d886bdbc9812094c5f3624fb0e2cf))
* on change issue ([80c8a50](https://gitlab.com/appointy/waqt/ui/service2/commit/80c8a50d1ec408bd0900a62a3d88477100567a1e))

### [0.2.218](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.217...v0.2.218) (2020-11-07)


### Features

* customer bulk action ([07dbfa8](https://gitlab.com/appointy/waqt/ui/service2/commit/07dbfa8009aa78d7ef3f043b5b8abe9116986d27))

### [0.2.217](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.216...v0.2.217) (2020-11-06)


### Features

* customer setting hook take one ([7e57842](https://gitlab.com/appointy/waqt/ui/service2/commit/7e578423c401e3875bc603fa4b5cdd4d7174d270))

### [0.2.216](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.215...v0.2.216) (2020-11-06)


### Bug Fixes

* booking rules tax widget empty value ([95edb7a](https://gitlab.com/appointy/waqt/ui/service2/commit/95edb7ac5dcbbb8d9a2d8370c160d406d9ae9af9))
* padding and message color ([a429b3a](https://gitlab.com/appointy/waqt/ui/service2/commit/a429b3a1b9d8290403489697db37ff5c301e4877))
* service access subtitle ([ca24d14](https://gitlab.com/appointy/waqt/ui/service2/commit/ca24d142fa1bd3f717274a15aff2e247028b68a1))
* tax widget empty value ([52327c0](https://gitlab.com/appointy/waqt/ui/service2/commit/52327c0b2c1642286280839c2c66878c11dd0565))

### [0.2.215](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.214...v0.2.215) (2020-11-06)


### Features

* customer restrictions ([233d7f8](https://gitlab.com/appointy/waqt/ui/service2/commit/233d7f80a721fcc1862f4566c606d4ed640d48b1))

### [0.2.214](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.213...v0.2.214) (2020-11-05)


### Features

* customer restriction widget ([594fe0e](https://gitlab.com/appointy/waqt/ui/service2/commit/594fe0eb4906cc0d8355357609f36c4965e72a96))

### [0.2.213](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.212...v0.2.213) (2020-11-05)


### Bug Fixes

* booking restictions ([5ed907d](https://gitlab.com/appointy/waqt/ui/service2/commit/5ed907d6b551152fd209e730f0892da6f314850a))

### [0.2.212](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.211...v0.2.212) (2020-11-04)


### Bug Fixes

* filters ([a992c46](https://gitlab.com/appointy/waqt/ui/service2/commit/a992c46bf65f628751d62073cc2183aaacf6f5c9))

### [0.2.211](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.210...v0.2.211) (2020-11-04)


### Features

* update changed settings ([bd138af](https://gitlab.com/appointy/waqt/ui/service2/commit/bd138af718f3cbb0afd54084743084db45cdccf3))
* update changed settings ([bf66c08](https://gitlab.com/appointy/waqt/ui/service2/commit/bf66c081cc2943144c4e05399ca5e1bb782a08d1))
* update changed settings take one ([ca272b2](https://gitlab.com/appointy/waqt/ui/service2/commit/ca272b221158e18c44508250f0752672af760715))

### [0.2.210](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.209...v0.2.210) (2020-11-04)


### Bug Fixes

* linking ui ([0434c50](https://gitlab.com/appointy/waqt/ui/service2/commit/0434c50f52956f20c3eeb2e2f973c9b245e90789))

### [0.2.209](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.208...v0.2.209) (2020-11-02)


### Bug Fixes

* empty states ([842c052](https://gitlab.com/appointy/waqt/ui/service2/commit/842c052fa14b06a9d1207b542ef556e9f5eed1ae))

### [0.2.208](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.207...v0.2.208) (2020-10-31)


### Bug Fixes

* handle empty in linking ([477b42e](https://gitlab.com/appointy/waqt/ui/service2/commit/477b42e9a32ce09452689a2defe89d956f4856c2))

### [0.2.207](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.206...v0.2.207) (2020-10-30)


### Bug Fixes

* minimum schedule notice ([d1a84dc](https://gitlab.com/appointy/waqt/ui/service2/commit/d1a84dc3aec4e80fc3e90fc311e6ed156c2c32fb))

### [0.2.206](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.205...v0.2.206) (2020-10-30)


### Bug Fixes

* interval cutoff widget ([85a68ca](https://gitlab.com/appointy/waqt/ui/service2/commit/85a68cac99d117acdd3e1596de4a197ecc27eb15))

### [0.2.205](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.204...v0.2.205) (2020-10-30)


### Bug Fixes

* bugs ([f0f97bf](https://gitlab.com/appointy/waqt/ui/service2/commit/f0f97bf6e9066e2dd33d433f6e7360a3b04b14df))

### [0.2.204](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.203...v0.2.204) (2020-10-30)


### Features

* assign services to staff ([6fd2f7d](https://gitlab.com/appointy/waqt/ui/service2/commit/6fd2f7da92ff2d24d3aa6a90052b9fa5084a5b57))
* category filter ([1b32666](https://gitlab.com/appointy/waqt/ui/service2/commit/1b32666e16803cbf3271122c48e44cde72b241c7))


### Bug Fixes

* service pricing ([262f656](https://gitlab.com/appointy/waqt/ui/service2/commit/262f656e3d306a0dc6dfda7baafc24ea122776de))
* text in international ([39e31df](https://gitlab.com/appointy/waqt/ui/service2/commit/39e31df12436d404f750ef771ee584a84baafc3d))

### [0.2.203](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.202...v0.2.203) (2020-10-29)


### Features

* assigned services in staff ([0bb4b24](https://gitlab.com/appointy/waqt/ui/service2/commit/0bb4b24f6b3d7051cc2643b3424358c8baf25e01))
* assigned services in staff ([995b0ee](https://gitlab.com/appointy/waqt/ui/service2/commit/995b0eeac87cd1458e4ffe036a9c6b5bb3d523b8))

### [0.2.202](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.201...v0.2.202) (2020-10-29)


### Bug Fixes

* step 5 ([313f303](https://gitlab.com/appointy/waqt/ui/service2/commit/313f303bcff199ad7c0c00c71e6fd63829e46c2f))
* summary in advance options ([fa71457](https://gitlab.com/appointy/waqt/ui/service2/commit/fa714579d4878473836c7a9baf5e5a71ae366c99))

### [0.2.201](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.200...v0.2.201) (2020-10-29)


### Bug Fixes

* summary in advance options ([60b98d8](https://gitlab.com/appointy/waqt/ui/service2/commit/60b98d805cf150d32fd8329cd209d32f9c15d561))

### [0.2.200](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.199...v0.2.200) (2020-10-28)


### Features

* summary take one ([b344760](https://gitlab.com/appointy/waqt/ui/service2/commit/b344760790a10824fbbec78e50859d36d24ee53c))


### Bug Fixes

* summary in advance options ([a39f74d](https://gitlab.com/appointy/waqt/ui/service2/commit/a39f74db0e3df65130fcd50bd94836f74e092074))

### [0.2.199](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.198...v0.2.199) (2020-10-28)


### Bug Fixes

* release issues ([679f3bf](https://gitlab.com/appointy/waqt/ui/service2/commit/679f3bf4155734352c0daa75cb2dd5da85fc34ff))

### [0.2.198](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.197...v0.2.198) (2020-10-28)


### Bug Fixes

* assign dialog ([f8bcede](https://gitlab.com/appointy/waqt/ui/service2/commit/f8bcede7288907a8a302a5e7ba4d3aeac2b7ddf7))
* checkbox align ([4d983db](https://gitlab.com/appointy/waqt/ui/service2/commit/4d983db3dbeb7f08baeabfa2f7e4107e18cbcccb))
* default fixed interval ([4d17509](https://gitlab.com/appointy/waqt/ui/service2/commit/4d175092988f256d06930176bddb0f3a0bac64a4))
* detect timezone for service ([de304ca](https://gitlab.com/appointy/waqt/ui/service2/commit/de304ca43e85935c4228ffe84c7a4ce1fc38badd))
* employee assign align ([4b5ab0c](https://gitlab.com/appointy/waqt/ui/service2/commit/4b5ab0c2f8b400d28c421aaa6582bf62c4db7301))
* employee assign dialog ([f749e6c](https://gitlab.com/appointy/waqt/ui/service2/commit/f749e6c6e4c7b8eb94c73250930bdf3f6d77cadd))
* internal ([a542f97](https://gitlab.com/appointy/waqt/ui/service2/commit/a542f972a86621c7b38081a864942208a7ef6f8f))
* lead and cancellation alignment ([2248da8](https://gitlab.com/appointy/waqt/ui/service2/commit/2248da8d0dc7cbe37cb9ee0f8b52ac7c85208cc5))
* loader in search ([48c8996](https://gitlab.com/appointy/waqt/ui/service2/commit/48c899663bb56afc6917af2b4fb65ea5777c9d13))

### [0.2.197](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.196...v0.2.197) (2020-10-28)


### Bug Fixes

* international widget ([65351bc](https://gitlab.com/appointy/waqt/ui/service2/commit/65351bca7e93b60237fe77cd7dd80de11cc32aea))

### [0.2.196](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.195...v0.2.196) (2020-10-28)


### Features

* dialog container help text ([f5ebeac](https://gitlab.com/appointy/waqt/ui/service2/commit/f5ebeaca15f94d9d7a6aa163867bdb283b3ec8e7))

### [0.2.195](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.194...v0.2.195) (2020-10-28)


### Features

* service filter on setting ([4f12f9e](https://gitlab.com/appointy/waqt/ui/service2/commit/4f12f9e20958648ff60ca32ef95dc8a26304e3c0))
* staff restriction warning ([7875062](https://gitlab.com/appointy/waqt/ui/service2/commit/78750626958355ad670b32efd246f2de0e0e291e))
* update capacity ([e0926d9](https://gitlab.com/appointy/waqt/ui/service2/commit/e0926d9baaad51efbe8acbdf65a7efbb579e9642))

### [0.2.194](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.193...v0.2.194) (2020-10-28)


### Bug Fixes

* update masks ([c3af1a0](https://gitlab.com/appointy/waqt/ui/service2/commit/c3af1a0f13a2f3fe931d2f04fb35d36915aeae9a))

### [0.2.193](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.192...v0.2.193) (2020-10-26)


### Bug Fixes

* pass filters in pagination query ([d646141](https://gitlab.com/appointy/waqt/ui/service2/commit/d646141a23296ada6f0974028c2a571d2caf4ce0))

### [0.2.192](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.191...v0.2.192) (2020-10-25)


### Features

* filters ([49017cf](https://gitlab.com/appointy/waqt/ui/service2/commit/49017cf0213d9bb3d0107bcff138ba749ea3f735))

### [0.2.191](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.190...v0.2.191) (2020-10-25)


### Bug Fixes

* responsive search ([0ff6f60](https://gitlab.com/appointy/waqt/ui/service2/commit/0ff6f6035f1e678287322c427b11bd237baf2b3f))

### [0.2.190](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.189...v0.2.190) (2020-10-25)


### Bug Fixes

* internationalization ([89f80a6](https://gitlab.com/appointy/waqt/ui/service2/commit/89f80a67724df50bb01f02c0320a77ccb09ac456))
* responsive ([899710d](https://gitlab.com/appointy/waqt/ui/service2/commit/899710d7c8b143e8bf3f0892b89f8814f9fcb4f1))

### [0.2.189](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.188...v0.2.189) (2020-10-25)


### Features

* copy url ([b54d560](https://gitlab.com/appointy/waqt/ui/service2/commit/b54d56067d06483eb95337fa1a7192c11cbe0ef2))
* copy url ([dda610b](https://gitlab.com/appointy/waqt/ui/service2/commit/dda610b2c06974499dbdb866b59ee91c52090715))

### [0.2.188](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.187...v0.2.188) (2020-10-25)


### Features

* location level in booking rules ([8e33e6e](https://gitlab.com/appointy/waqt/ui/service2/commit/8e33e6e4ce0f8c23bde405ca6b8ff6d458c491ff))

### [0.2.187](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.186...v0.2.187) (2020-10-25)


### Bug Fixes

* step 3 ([0b69bcd](https://gitlab.com/appointy/waqt/ui/service2/commit/0b69bcdb452093096a73dab5f1b6c29b8dbd87b7))

### [0.2.186](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.185...v0.2.186) (2020-10-25)


### Features

* pricing update ([9886321](https://gitlab.com/appointy/waqt/ui/service2/commit/9886321399e763f5323d38456cf015cc6c56a4e9))

### [0.2.185](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.184...v0.2.185) (2020-10-25)


### Bug Fixes

* mutation ([2a75dfb](https://gitlab.com/appointy/waqt/ui/service2/commit/2a75dfb12c29ea3d096e7d2331d04ceeaa08de78))

### [0.2.184](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.183...v0.2.184) (2020-10-25)


### Bug Fixes

* linking issues ([0a4f57b](https://gitlab.com/appointy/waqt/ui/service2/commit/0a4f57bb30b7937519944a654d95454b93ccffd5))

### [0.2.183](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.182...v0.2.183) (2020-10-24)


### Bug Fixes

* release issues ([709e6b4](https://gitlab.com/appointy/waqt/ui/service2/commit/709e6b4c53af13e603768b710a2b9214bf62edcb))

### [0.2.182](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.181...v0.2.182) (2020-10-24)


### Features

* assign staff ([9805bf8](https://gitlab.com/appointy/waqt/ui/service2/commit/9805bf8f9d4498d1b6970dd6bda4a9bc2d0060b7))
* assign staff ([7110e8d](https://gitlab.com/appointy/waqt/ui/service2/commit/7110e8d755a6758247127245b0fc0ac5f578d050))

### [0.2.181](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.180...v0.2.181) (2020-10-24)


### Bug Fixes

* booking rules recurring padding ([8708eb5](https://gitlab.com/appointy/waqt/ui/service2/commit/8708eb5d623d589ba010a3018ead52f2992d31eb))

### [0.2.180](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.179...v0.2.180) (2020-10-24)


### Bug Fixes

* responsiveness ([8ab3505](https://gitlab.com/appointy/waqt/ui/service2/commit/8ab3505403989597e11dce7ce512d6d3468269a7))

### [0.2.179](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.178...v0.2.179) (2020-10-24)


### Features

* delete linking ([c11e982](https://gitlab.com/appointy/waqt/ui/service2/commit/c11e982e78d2a2f64141ab32acfb7c24cfa253f2))
* employees of service take one ([722ff76](https://gitlab.com/appointy/waqt/ui/service2/commit/722ff76caa8cdd5470e3f2512ce8e22b94efbd62))

### [0.2.178](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.177...v0.2.178) (2020-10-24)


### Bug Fixes

* booking rules ([9888ee9](https://gitlab.com/appointy/waqt/ui/service2/commit/9888ee9d6cb0dd1ba69c30481b92e33a24c64e8d))

### [0.2.177](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.176...v0.2.177) (2020-10-24)


### Features

* reset to location ([0cd4284](https://gitlab.com/appointy/waqt/ui/service2/commit/0cd428461676ee3095a88331bb8f79d509a034f7))

### [0.2.176](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.175...v0.2.176) (2020-10-23)


### Features

* lead and cancellation reset ([79fbee3](https://gitlab.com/appointy/waqt/ui/service2/commit/79fbee388c79ef1d1713427fcc8d664bbd0e044c))

### [0.2.175](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.174...v0.2.175) (2020-10-23)


### Features

* reset to location spam settings ([b462a35](https://gitlab.com/appointy/waqt/ui/service2/commit/b462a35addd99ae28d3744fb0a6b91f6f50052f1))
* spam settings ui responsive ([8579c46](https://gitlab.com/appointy/waqt/ui/service2/commit/8579c46b01a8149110c85572fab4d74e519413a8))

### [0.2.174](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.173...v0.2.174) (2020-10-23)


### Features

* international widget ([c650ee0](https://gitlab.com/appointy/waqt/ui/service2/commit/c650ee06c293e0ce8695c35565aae181b9116aad))

### [0.2.173](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.172...v0.2.173) (2020-10-23)


### Features

* timezone restrictions in booking rules ([362a62b](https://gitlab.com/appointy/waqt/ui/service2/commit/362a62bfc23ec55a318943dd918e2d20147136a9))
* timezone restrictions in service settings ([377890e](https://gitlab.com/appointy/waqt/ui/service2/commit/377890e843e7a009a9fc00bb1a0419c83b447128))

### [0.2.172](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.171...v0.2.172) (2020-10-23)


### Features

* payment settings ([b2f98ac](https://gitlab.com/appointy/waqt/ui/service2/commit/b2f98ac1b1647f9c780864e0b4f1fd24783e3b19))
* payment settings in action ([5a97434](https://gitlab.com/appointy/waqt/ui/service2/commit/5a97434cb2441fa0393b7fadbae5e08d5b0f3d79))


### Bug Fixes

* tax settings ([67b53b8](https://gitlab.com/appointy/waqt/ui/service2/commit/67b53b8f05cfe69a1d8c87327858d013b8d36842))

### [0.2.171](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.170...v0.2.171) (2020-10-23)


### Bug Fixes

* action ([2705bee](https://gitlab.com/appointy/waqt/ui/service2/commit/2705bee76dde2cc65bcf9c2afa00c17acb700bdd))
* update policy widget ([a5df681](https://gitlab.com/appointy/waqt/ui/service2/commit/a5df681dda98ed740a3717598026877215257294))
* update policy widget ([4342d35](https://gitlab.com/appointy/waqt/ui/service2/commit/4342d3505ec04b3849cc0473ab0c9d6323110afa))

### [0.2.170](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.169...v0.2.170) (2020-10-23)


### Bug Fixes

* booking rules ui ([2fd2b1e](https://gitlab.com/appointy/waqt/ui/service2/commit/2fd2b1e818ae16f1e4c82c3b51750e153230fc3c))

### [0.2.169](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.168...v0.2.169) (2020-10-23)


### Features

* multiple items ([97a03e0](https://gitlab.com/appointy/waqt/ui/service2/commit/97a03e01b9bdb3214c3edd57149fe23d441f0e0c))


### Bug Fixes

* booking rules ui ([fede9e9](https://gitlab.com/appointy/waqt/ui/service2/commit/fede9e98c8ccaecf7aea4350cb41e54e29ee6633))
* booking rules ui ([eb1b009](https://gitlab.com/appointy/waqt/ui/service2/commit/eb1b0099a442e7165ea68c5486779041371480e5))
* tax settings multiple items ([5e9d92e](https://gitlab.com/appointy/waqt/ui/service2/commit/5e9d92e7c58d10d44b679c58321b97dcb53e4372))

### [0.2.168](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.167...v0.2.168) (2020-10-22)


### Features

* dialog container ([6788f71](https://gitlab.com/appointy/waqt/ui/service2/commit/6788f71a665b9369901ee106617cfa3b256be193))
* dialog container for spam settings take one ([34c3e7a](https://gitlab.com/appointy/waqt/ui/service2/commit/34c3e7ae9ba3f829411b61c09b84cdb4c38615a3))

### [0.2.167](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.166...v0.2.167) (2020-10-21)


### Features

* booking restrictions ([09c3d94](https://gitlab.com/appointy/waqt/ui/service2/commit/09c3d9447c85c2cfa66005d77fcd00627d08e6f7))


### Bug Fixes

* fixed virtual services ([05f7ad1](https://gitlab.com/appointy/waqt/ui/service2/commit/05f7ad1678bc6bd6099836176579aa4c6deb08ca))

### [0.2.166](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.165...v0.2.166) (2020-10-21)


### Bug Fixes

* fixed virtual services ([05f7ad1](https://gitlab.com/appointy/waqt/ui/service2/commit/05f7ad1678bc6bd6099836176579aa4c6deb08ca))

### [0.2.165](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.164...v0.2.165) (2020-10-21)


### Features

* recurring booking ([24e7348](https://gitlab.com/appointy/waqt/ui/service2/commit/24e734877b9489642d2601121f7dffd8b673b816))

### [0.2.164](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.163...v0.2.164) (2020-10-21)


### Features

* booking rules payment settings ([faa7296](https://gitlab.com/appointy/waqt/ui/service2/commit/faa7296c6eee6090fb9f234b0f9fdcdf5a10198a))

### [0.2.163](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.162...v0.2.163) (2020-10-21)


### Bug Fixes

* release issues ([e3af082](https://gitlab.com/appointy/waqt/ui/service2/commit/e3af08285de804b5b63732e27ec6e1cc3d05b885))

### [0.2.162](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.161...v0.2.162) (2020-10-21)


### Features

* tax settings ([08829c5](https://gitlab.com/appointy/waqt/ui/service2/commit/08829c567e5f8ddb3c2142cc8368e8c5572c560e))

### [0.2.161](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.160...v0.2.161) (2020-10-21)


### Features

* lead and cancellation ([e801635](https://gitlab.com/appointy/waqt/ui/service2/commit/e8016359bf440b9d5a6178cb0b5746531c0e8c66))


### Bug Fixes

* issues ([3693e47](https://gitlab.com/appointy/waqt/ui/service2/commit/3693e47e9ab24045c05148a6fab7fa74e8478f43))

### [0.2.160](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.159...v0.2.160) (2020-10-21)


### Features

* booking rules spam settings ([fb70d24](https://gitlab.com/appointy/waqt/ui/service2/commit/fb70d2481e7d3edcb4e44f44bb9ea10d04d68bd9))

### [0.2.159](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.158...v0.2.159) (2020-10-21)


### Bug Fixes

* release issue ([3b3d5d2](https://gitlab.com/appointy/waqt/ui/service2/commit/3b3d5d248db7bd8eabab018447d5bc2cd9d1afca))

### [0.2.158](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.157...v0.2.158) (2020-10-21)


### Features

* cancellation notice ([ef03a47](https://gitlab.com/appointy/waqt/ui/service2/commit/ef03a47e9ebf5ecb0283a6c886a65eaee081be2c))
* prepayment info ([4f5c998](https://gitlab.com/appointy/waqt/ui/service2/commit/4f5c99844f72bfaf51572f8382b5bb9cf496f132))
* service location ([b30ff70](https://gitlab.com/appointy/waqt/ui/service2/commit/b30ff70b4142b8ed0b21c90527fa65a9ac65cd1b))


### Bug Fixes

* service settings query ([293ae5b](https://gitlab.com/appointy/waqt/ui/service2/commit/293ae5b30ef98822289bcd938f07e365fa85d564))

### [0.2.157](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.156...v0.2.157) (2020-10-21)


### Features

* customer restrictions ([6746957](https://gitlab.com/appointy/waqt/ui/service2/commit/674695719f533096e3ad5e0713060837919f990e))
* customer restrictions take one ([06bc1f3](https://gitlab.com/appointy/waqt/ui/service2/commit/06bc1f3fcaee36a68eb107f098ad6d2a442ee9c6))
* customer restrictions take two ([8679fba](https://gitlab.com/appointy/waqt/ui/service2/commit/8679fbaadb3f61b3e1f2b779d4f3a8f111a4e1a6))

### [0.2.156](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.155...v0.2.156) (2020-10-21)


### Bug Fixes

* lead and cancellation ([6106259](https://gitlab.com/appointy/waqt/ui/service2/commit/6106259788a3052aac9b6dd624e49194b870b735))

### [0.2.155](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.154...v0.2.155) (2020-10-21)


### Features

* spam protection in service ([55d1613](https://gitlab.com/appointy/waqt/ui/service2/commit/55d1613d4b9941b6c806ad9b637facc44575f336))

### [0.2.154](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.153...v0.2.154) (2020-10-21)


### Features

* cancellation option ([ecdf685](https://gitlab.com/appointy/waqt/ui/service2/commit/ecdf6854fdd09a35eff4e18fb9a05571e4c7e73a))
* lead and cancellation complete ([19052b3](https://gitlab.com/appointy/waqt/ui/service2/commit/19052b3800af5f00a8b55ccdaeb71f32e5c82b3f))

### [0.2.153](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.152...v0.2.153) (2020-10-20)


### Bug Fixes

* build issues ([cd94759](https://gitlab.com/appointy/waqt/ui/service2/commit/cd94759c9e91f47371548630c06e6177b9a6f0a5))

### [0.2.152](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.151...v0.2.152) (2020-10-20)


### Features

* basic lead and cancellation ([7901b8b](https://gitlab.com/appointy/waqt/ui/service2/commit/7901b8b257eb92091ac873bf4d730325d696f74a))
* basic lead and cancellation ([33619b1](https://gitlab.com/appointy/waqt/ui/service2/commit/33619b13af110ab603ac2bb12eec29858f72e24a))
* lead and cancellation take one ([ef7d29a](https://gitlab.com/appointy/waqt/ui/service2/commit/ef7d29aa4c37e1880dfab6b9d760dde7e78ea315))

### [0.2.151](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.150...v0.2.151) (2020-10-20)


### Features

* advance options AvailabilityOptions.tsx ([fc955d0](https://gitlab.com/appointy/waqt/ui/service2/commit/fc955d0be0baa77f9ad3e2b390471b9b6cd82a49))
* advance settings ([db3acf1](https://gitlab.com/appointy/waqt/ui/service2/commit/db3acf15f3a2b19084137c1ef3944bf2198e046f))

### [0.2.150](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.149...v0.2.150) (2020-10-20)


### Features

* internation restrictions ([cf5053d](https://gitlab.com/appointy/waqt/ui/service2/commit/cf5053dc3a2b368f0c08f5941e5f36ce4c859df3))

### [0.2.149](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.148...v0.2.149) (2020-10-20)


### Features

* payment settings ([c10194f](https://gitlab.com/appointy/waqt/ui/service2/commit/c10194f5af5a802868fb407ff230bf736973b093))

### [0.2.148](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.147...v0.2.148) (2020-10-20)


### Features

* tax settings ([90d50ad](https://gitlab.com/appointy/waqt/ui/service2/commit/90d50ad13e8be37c0d5ad3798059c18e173f2da9))


### Bug Fixes

* service access ([eb75b75](https://gitlab.com/appointy/waqt/ui/service2/commit/eb75b75543e1942cc825505d4742b0efee964e12))
* tax settings ([2a8814d](https://gitlab.com/appointy/waqt/ui/service2/commit/2a8814dfc3e6746ef029d5e7151faa2c751178f0))
* tax settings ([afc7075](https://gitlab.com/appointy/waqt/ui/service2/commit/afc707543853dd52595c0aa25b0d280372876809))
* tax widget ([c5e7c85](https://gitlab.com/appointy/waqt/ui/service2/commit/c5e7c85bb254f0a42b47245b814b45fc5d4f5fb6))

### [0.2.147](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.146...v0.2.147) (2020-10-20)


### Bug Fixes

* card component ([f4be3b4](https://gitlab.com/appointy/waqt/ui/service2/commit/f4be3b4653badcb2a92cefda275fa415e1079654))

### [0.2.146](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.145...v0.2.146) (2020-10-20)


### Features

* time buffer take one ([7e0dfc7](https://gitlab.com/appointy/waqt/ui/service2/commit/7e0dfc73f26fe222600cfc26be0b2fe5ace9be03))
* time buffer take two ([fc8025f](https://gitlab.com/appointy/waqt/ui/service2/commit/fc8025f9ffbc56069d4cb3c9a1b4f3217ddaaa01))

### [0.2.145](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.144...v0.2.145) (2020-10-20)


### Features

* update policy action ([697a05a](https://gitlab.com/appointy/waqt/ui/service2/commit/697a05a7b4c4bed3c977fac9149512fdbdddb297))

### [0.2.144](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.143...v0.2.144) (2020-10-19)


### Features

* update policy take one ([80aa52d](https://gitlab.com/appointy/waqt/ui/service2/commit/80aa52d7b83f36136d70cd41dd48a97efbac3ad0))

### [0.2.143](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.142...v0.2.143) (2020-10-19)


### Features

* cutoff widget ([4b43480](https://gitlab.com/appointy/waqt/ui/service2/commit/4b434802766518bac7b16161e5134c9760589c81))

### [0.2.142](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.141...v0.2.142) (2020-10-19)


### Features

* payment settings ([4af021d](https://gitlab.com/appointy/waqt/ui/service2/commit/4af021d2d42e976fe539bcd30f088a0cec60cc1e))
* tax settings ([99e64b7](https://gitlab.com/appointy/waqt/ui/service2/commit/99e64b7ddd29440b3a49faa50f13ce8879dc3652))
* upsert mutation take one ([25a5ee9](https://gitlab.com/appointy/waqt/ui/service2/commit/25a5ee9bb2b562768cd62fc4870378fbc81b4e3c))

### [0.2.141](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.140...v0.2.141) (2020-10-17)


### Features

* location, access settings ([8b29f0b](https://gitlab.com/appointy/waqt/ui/service2/commit/8b29f0bba8a03efa7be5d4b834f2d85df713c399))


### Bug Fixes

* ui ([369205a](https://gitlab.com/appointy/waqt/ui/service2/commit/369205af161d2fd10c6a5e0acb03884e9088cb68))

### [0.2.140](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.139...v0.2.140) (2020-10-17)


### Features

* form changes ([f9cfa40](https://gitlab.com/appointy/waqt/ui/service2/commit/f9cfa40e6829a80423452f00f2f73bb3846caadc))

### [0.2.139](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.138...v0.2.139) (2020-10-17)


### Features

* initial step 2 ([ebf5923](https://gitlab.com/appointy/waqt/ui/service2/commit/ebf59230678b147420463173eb20c05036b29e8d))

### [0.2.138](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.137...v0.2.138) (2020-10-17)


### Features

* initial step 1 ([4a5db5d](https://gitlab.com/appointy/waqt/ui/service2/commit/4a5db5d7a9aea12269f706c0f77b326be7aa5ea7))

### [0.2.137](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.136...v0.2.137) (2020-10-17)


### Bug Fixes

* validations ([92a45d8](https://gitlab.com/appointy/waqt/ui/service2/commit/92a45d83c497ce2dc86485099ec719199c8881af))

### [0.2.136](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.135...v0.2.136) (2020-10-16)


### Bug Fixes

* release issues ([806aee0](https://gitlab.com/appointy/waqt/ui/service2/commit/806aee0ac27e3627a267daab3264038374b18941))

### [0.2.135](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.134...v0.2.135) (2020-10-16)


### Features

* prepayment action ([088ab9b](https://gitlab.com/appointy/waqt/ui/service2/commit/088ab9bca74147affc418e7408d6593a0acc6066))


### Bug Fixes

* tax rate ([9ade5d5](https://gitlab.com/appointy/waqt/ui/service2/commit/9ade5d577cf8ab3d5f0ffdadbab2f3d32ccaa818))

### [0.2.134](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.133...v0.2.134) (2020-10-16)


### Features

* service actions working ([2857fb6](https://gitlab.com/appointy/waqt/ui/service2/commit/2857fb6b965732da4af9efeb2182432d9e7ab9b5))

### [0.2.133](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.132...v0.2.133) (2020-10-16)


### Features

* tax widget with validations ([e7032f6](https://gitlab.com/appointy/waqt/ui/service2/commit/e7032f628c1b90edaf878f259f4aa2443b92aae5))

### [0.2.132](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.131...v0.2.132) (2020-10-16)


### Features

* tax widget without validations ([a837669](https://gitlab.com/appointy/waqt/ui/service2/commit/a83766970eecd5b3636733866a751440eb1ecac3))

### [0.2.131](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.130...v0.2.131) (2020-10-16)


### Features

* visibility action ([7448a3c](https://gitlab.com/appointy/waqt/ui/service2/commit/7448a3cd349aa549436b472047288cf0919ee5c6))

### [0.2.130](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.129...v0.2.130) (2020-10-16)


### Features

* actions structure ([9474297](https://gitlab.com/appointy/waqt/ui/service2/commit/94742975e22f05f571027904fb60d65a76c01912))
* service status action ([aef7370](https://gitlab.com/appointy/waqt/ui/service2/commit/aef7370d89604ddef705afeedeab7af1bfae0877))

### [0.2.129](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.128...v0.2.129) (2020-10-09)


### Bug Fixes

* remove booking limits and notification settings ([e6886b7](https://gitlab.com/appointy/waqt/ui/service2/commit/e6886b758ee3676a07e0687775894084adebc9da))

### [0.2.128](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.127...v0.2.128) (2020-10-09)


### Bug Fixes

* discount summary ([535dfa6](https://gitlab.com/appointy/waqt/ui/service2/commit/535dfa637efd3d7dca71e474b71e6ba01f4e3a87))

### [0.2.127](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.126...v0.2.127) (2020-10-09)


### Features

* discount summary ([d038b30](https://gitlab.com/appointy/waqt/ui/service2/commit/d038b3030822c011c29c7eb7183949b07f92af31))

### [0.2.126](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.125...v0.2.126) (2020-10-06)


### Bug Fixes

* discount text ([30d54df](https://gitlab.com/appointy/waqt/ui/service2/commit/30d54dfcb3799821cf4c5d6780194968329f5b4b))

### [0.2.125](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.124...v0.2.125) (2020-10-06)


### Bug Fixes

* discount text ([7ed2381](https://gitlab.com/appointy/waqt/ui/service2/commit/7ed2381f6459588fdb50c05b81b3ab1c1c45e52d))

### [0.2.124](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.123...v0.2.124) (2020-10-06)


### Bug Fixes

* type custom ([89a7c4f](https://gitlab.com/appointy/waqt/ui/service2/commit/89a7c4f04ee24d047dd3959b4969e4a14f3d953d))

### [0.2.123](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.122...v0.2.123) (2020-10-05)


### Bug Fixes

* discountable item summary ([8c45cea](https://gitlab.com/appointy/waqt/ui/service2/commit/8c45ceaa78ab81fd2fb93d2d392d09c275f0effc))

### [0.2.122](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.121...v0.2.122) (2020-10-03)


### Features

* rights implemented ([a39a424](https://gitlab.com/appointy/waqt/ui/service2/commit/a39a42431e79b83459ec5f394a1f14d2fd981384))

### [0.2.121](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.120...v0.2.121) (2020-10-03)


### Features

* employee restrictions ([156bda4](https://gitlab.com/appointy/waqt/ui/service2/commit/156bda4df6a01c3eda94e13da92010869c190b65))

### [0.2.120](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.119...v0.2.120) (2020-10-03)


### Features

* employee restrictions ([aca4438](https://gitlab.com/appointy/waqt/ui/service2/commit/aca4438ae43428cdbfd01901078b41f7101e62f1))

### [0.2.119](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.118...v0.2.119) (2020-10-01)


### Features

* booking restriction options ([676a895](https://gitlab.com/appointy/waqt/ui/service2/commit/676a89537043b166640260bbf74a1b4750aafa66))

### [0.2.118](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.117...v0.2.118) (2020-10-01)


### Features

* new spam settings summary ([c3f1729](https://gitlab.com/appointy/waqt/ui/service2/commit/c3f172949d884e683984dbfa0970ee3696d92e12))
* spam settings checkbox ([c4fffcf](https://gitlab.com/appointy/waqt/ui/service2/commit/c4fffcf8b04fee84a264585b3a687d88236ae993))

### [0.2.117](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.116...v0.2.117) (2020-10-01)


### Features

* checklist ([6b81745](https://gitlab.com/appointy/waqt/ui/service2/commit/6b817457e401f987c90da8be7db42bb52a513522))
* service settings responsive ([02a469e](https://gitlab.com/appointy/waqt/ui/service2/commit/02a469e5c87ef5d1f9ed2c53b274905d2d62e7da))

### [0.2.116](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.115...v0.2.116) (2020-10-01)


### Features

* negative validations ([3345421](https://gitlab.com/appointy/waqt/ui/service2/commit/33454210f30d229c0c7fd5ed1380522edfb71694))

### [0.2.115](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.114...v0.2.115) (2020-10-01)


### Bug Fixes

* lead and cancellation ([9a5732a](https://gitlab.com/appointy/waqt/ui/service2/commit/9a5732a06e63ce7c5eabc5cff2aba2873ae7ee85))
* payment settings ([aefa9fe](https://gitlab.com/appointy/waqt/ui/service2/commit/aefa9fe4384758ec3eaf478ea658c7a809c96da8))
* test ([94d7d0a](https://gitlab.com/appointy/waqt/ui/service2/commit/94d7d0a3566ae47026f2cc5bdc5ef847e1c2ac5d))

### [0.2.114](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.113...v0.2.114) (2020-09-30)


### Bug Fixes

* remove staff restrictions ([1e43a58](https://gitlab.com/appointy/waqt/ui/service2/commit/1e43a588ea214147ce216db29dcfaceaaf40de1d))

### [0.2.113](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.112...v0.2.113) (2020-09-23)


### Bug Fixes

* linked service actions ([c685fbf](https://gitlab.com/appointy/waqt/ui/service2/commit/c685fbf5134ad446c917c0f6aed5a06e09a9e35f))

### [0.2.112](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.111...v0.2.112) (2020-09-16)


### Bug Fixes

* customer portal link to settings ([e9770c1](https://gitlab.com/appointy/waqt/ui/service2/commit/e9770c14c26e48c6e457d7f117474a93314685ad))

### [0.2.111](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.110...v0.2.111) (2020-09-16)


### Bug Fixes

* customer portal link to settings ([66dfe6c](https://gitlab.com/appointy/waqt/ui/service2/commit/66dfe6cd9102f007bad1a65d5f046fad64cf1a53))

### [0.2.110](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.109...v0.2.110) (2020-09-10)


### Bug Fixes

* category issue ([ee99996](https://gitlab.com/appointy/waqt/ui/service2/commit/ee99996bac37976a70557b3b8465cca771959f64))

### [0.2.109](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.108...v0.2.109) (2020-09-10)


### Bug Fixes

* do not show inactive services ([6c9e3e1](https://gitlab.com/appointy/waqt/ui/service2/commit/6c9e3e155329f90b45a83c7051f3c1a2446a0895))

### [0.2.108](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.107...v0.2.108) (2020-09-09)


### Bug Fixes

* message ([f8023bc](https://gitlab.com/appointy/waqt/ui/service2/commit/f8023bc40f0460ecf40f302f1e61098546099c7e))

### [0.2.107](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.106...v0.2.107) (2020-09-09)


### Bug Fixes

* chip input ([3d89f49](https://gitlab.com/appointy/waqt/ui/service2/commit/3d89f49883af8e45c220d6259ec9e584bf19e313))

### [0.2.106](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.105...v0.2.106) (2020-09-09)


### Bug Fixes

* navigate back ([714a05c](https://gitlab.com/appointy/waqt/ui/service2/commit/714a05c8f71fa1fbab8e97b76dcb0f9e68bd3428))

### [0.2.105](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.104...v0.2.105) (2020-09-09)


### Features

* service booking limits ([18ae128](https://gitlab.com/appointy/waqt/ui/service2/commit/18ae1280070a5fa6ddb02325ffd2255f6db18433))
* service booking limits ([a718110](https://gitlab.com/appointy/waqt/ui/service2/commit/a718110d44c0f10b3917d7c89b1f70d16cf5e16e))


### Bug Fixes

* lead cancellation default ([7313dc4](https://gitlab.com/appointy/waqt/ui/service2/commit/7313dc441ef854d4d0a7d0408bd79a5a9108ae59))
* update customer level settings ([7c80433](https://gitlab.com/appointy/waqt/ui/service2/commit/7c804339c69c3f588dafd5937dfb19f7199758bd))
* update customer level settings ([a16082b](https://gitlab.com/appointy/waqt/ui/service2/commit/a16082b3f767419d74ceccde0e1a6dcc132b3e2c))

### [0.2.104](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.103...v0.2.104) (2020-09-09)


### Bug Fixes

* booking restrictions in service ([a49895c](https://gitlab.com/appointy/waqt/ui/service2/commit/a49895cb332a12518cb32ca67e8bf4330262ae99))

### [0.2.103](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.102...v0.2.103) (2020-09-09)


### Bug Fixes

* booking restrictions in service ([c871271](https://gitlab.com/appointy/waqt/ui/service2/commit/c871271c1d624b4d1ee41f27df9389d8a3d780ac))

### [0.2.102](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.101...v0.2.102) (2020-09-08)


### Features

* booking restrictions ([e59260e](https://gitlab.com/appointy/waqt/ui/service2/commit/e59260e8ba8fc349bb8a933828e6c82ae6734693))
* booking restrictions ([20002f3](https://gitlab.com/appointy/waqt/ui/service2/commit/20002f3b9574c0fafc499fda5253cb88a75316d7))

### [0.2.101](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.100...v0.2.101) (2020-09-08)


### Features

* lead and cancellation fix ([46025ff](https://gitlab.com/appointy/waqt/ui/service2/commit/46025ff6dff069a9ad3b36240303699bc215f94b))
* spam settings ([e66f559](https://gitlab.com/appointy/waqt/ui/service2/commit/e66f5594adce550686a47ef988e54ba29cb78f1d))
* time settings fix ([4a5ca2a](https://gitlab.com/appointy/waqt/ui/service2/commit/4a5ca2a4d0f06282fb5bf6cf107758221edb4603))


### Bug Fixes

* advance options ([1a2293a](https://gitlab.com/appointy/waqt/ui/service2/commit/1a2293a6962d2dd6a4c7ccd52d7a5b4fa3acffc5))
* advance options ([afebe33](https://gitlab.com/appointy/waqt/ui/service2/commit/afebe330782c57c79bbc9fe1eb29892fdfc55a01))

### [0.2.100](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.99...v0.2.100) (2020-09-07)


### Bug Fixes

* booking limit text ([416511e](https://gitlab.com/appointy/waqt/ui/service2/commit/416511ee0997119e038d5b0100d9029ff0fd3fbc))
* limit on cancellation and refund policy percentage ([06b8ac0](https://gitlab.com/appointy/waqt/ui/service2/commit/06b8ac0599c62cf58031ec1f01ca7dde76abc223))

### [0.2.99](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.98...v0.2.99) (2020-09-07)


### Features

* inactive ([e968a4b](https://gitlab.com/appointy/waqt/ui/service2/commit/e968a4b1dd1e93d70d0d6575eefca1ad2af61ad0))

### [0.2.98](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.97...v0.2.98) (2020-09-07)


### Bug Fixes

* empty label ([0066d20](https://gitlab.com/appointy/waqt/ui/service2/commit/0066d20bf2b81ba21c32e35db06584fe83afd2ed))
* empty label ([1518f95](https://gitlab.com/appointy/waqt/ui/service2/commit/1518f952fc343e2b0e16842a6d5b7cb9216de17f))

### [0.2.97](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.96...v0.2.97) (2020-09-07)


### Bug Fixes

* discount header ([3fcb243](https://gitlab.com/appointy/waqt/ui/service2/commit/3fcb2431a926c16f3926cbaf91f7abfdee151227))

### [0.2.96](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.95...v0.2.96) (2020-09-05)


### Bug Fixes

* discount hook ([5d7d7be](https://gitlab.com/appointy/waqt/ui/service2/commit/5d7d7befd0529187f7ba8f7ffa05e0d92fa080bc))

### [0.2.95](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.94...v0.2.95) (2020-09-05)


### Bug Fixes

* discount hook ([bc40046](https://gitlab.com/appointy/waqt/ui/service2/commit/bc40046cce54c0c1f1c75c487f9181d8018cf9af))

### [0.2.94](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.93...v0.2.94) (2020-09-04)


### Bug Fixes

* discount hook ([dc314e9](https://gitlab.com/appointy/waqt/ui/service2/commit/dc314e98678b5cc5079dc029bb5fcf5d4c998b4b))
* linking ([8de1664](https://gitlab.com/appointy/waqt/ui/service2/commit/8de166485826608ec07731a37754875c8ecc81a1))

### [0.2.93](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.92...v0.2.93) (2020-09-03)


### Bug Fixes

* remove calendar url ([f1e8895](https://gitlab.com/appointy/waqt/ui/service2/commit/f1e8895f9c4e9501e7397663c1b0409b785f0e9b))

### [0.2.92](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.91...v0.2.92) (2020-09-03)


### Bug Fixes

* filters in discount hooks ([c2d03d3](https://gitlab.com/appointy/waqt/ui/service2/commit/c2d03d34ea9a1b9c34a85830dcdfb64ea6c36a0b))

### [0.2.91](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.90...v0.2.91) (2020-09-03)


### Bug Fixes

* filters in discount hooks ([7e2f7ee](https://gitlab.com/appointy/waqt/ui/service2/commit/7e2f7ee6115161d6737349a7585545fa386a9827))

### [0.2.90](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.89...v0.2.90) (2020-09-03)


### Features

* filters in discount hooks ([e212210](https://gitlab.com/appointy/waqt/ui/service2/commit/e2122100f20e2f1cf4f3576e7e5a9a7cb5b00325))

### [0.2.89](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.88...v0.2.89) (2020-09-02)


### Features

* groupby ([dc9832a](https://gitlab.com/appointy/waqt/ui/service2/commit/dc9832a2f146079b635826ceed6b49449049b008))

### [0.2.88](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.87...v0.2.88) (2020-09-02)


### Features

* groupby ([700c17e](https://gitlab.com/appointy/waqt/ui/service2/commit/700c17e03c7ee3c7a40bbc4dbbc90e526e0fb75a))

### [0.2.87](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.86...v0.2.87) (2020-08-31)


### Features

* subtitle ([483a9b6](https://gitlab.com/appointy/waqt/ui/service2/commit/483a9b62dd5ed58f93f359c1ae6f14da96ed9ba1))

### [0.2.86](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.85...v0.2.86) (2020-08-29)


### Features

* create service from service templates ([8cfc06f](https://gitlab.com/appointy/waqt/ui/service2/commit/8cfc06f3880f48dcc329d8e2432b497642334c3b))


### Bug Fixes

* create service from service template ([9fe8380](https://gitlab.com/appointy/waqt/ui/service2/commit/9fe83801cf0a695cac08fdfd7050fd19e62edcdd))

### [0.2.85](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.84...v0.2.85) (2020-08-28)


### Features

* booking rules roles in settings hook ([4dd7b19](https://gitlab.com/appointy/waqt/ui/service2/commit/4dd7b19a827b96746eba402cc0eda8c31f30509d))

### [0.2.84](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.83...v0.2.84) (2020-08-28)


### Features

* assign services of category ([3f35bd3](https://gitlab.com/appointy/waqt/ui/service2/commit/3f35bd304b7c3e091a5d43b481135c049371e56c))
* select services of category ([5fdf99a](https://gitlab.com/appointy/waqt/ui/service2/commit/5fdf99a8472523c7f43323897dfd8eb168752807))

### [0.2.83](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.82...v0.2.83) (2020-08-28)


### Features

* responsive linking ([76bf5ef](https://gitlab.com/appointy/waqt/ui/service2/commit/76bf5ef3ff4f50ae3da75a29d3347a7837c01762))
* responsive linking ([926e51e](https://gitlab.com/appointy/waqt/ui/service2/commit/926e51ebe4be05779d9254ceaeeaf34649ba7f60))

### [0.2.82](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.81...v0.2.82) (2020-08-28)


### Bug Fixes

* layoutProps ([07a61c1](https://gitlab.com/appointy/waqt/ui/service2/commit/07a61c1f27e7d183345244ed25937b8aa7a54a45))

### [0.2.81](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.80...v0.2.81) (2020-08-27)


### Bug Fixes

* ux responsive ([5ac2224](https://gitlab.com/appointy/waqt/ui/service2/commit/5ac2224f05a9884b4a4b249258cf5bab46b59ccc))

### [0.2.80](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.79...v0.2.80) (2020-08-27)


### Features

* category sorting ([10dd166](https://gitlab.com/appointy/waqt/ui/service2/commit/10dd16639e7c9cfc47eed2d7ef10564164714668))
* category sorting ([55a382b](https://gitlab.com/appointy/waqt/ui/service2/commit/55a382b0ec708b41d28e5fa0cabdefd80436f164))
* sorting ui done ([979d3b0](https://gitlab.com/appointy/waqt/ui/service2/commit/979d3b0d4ab81da6c17799532ef7a8b82a5df969))
* wrong algo ([462dd55](https://gitlab.com/appointy/waqt/ui/service2/commit/462dd55ae8c4d3ea26c63bf3b771ee59db57c0db))


### Bug Fixes

* logic for sorting in one category ([6e4366d](https://gitlab.com/appointy/waqt/ui/service2/commit/6e4366d576b8a378c23a696d814ec2cc4e46a4c3))
* service sorting working ([83f7352](https://gitlab.com/appointy/waqt/ui/service2/commit/83f73522d15c5b91aaaa02746a386d08124966a1))

### [0.2.79](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.78...v0.2.79) (2020-08-26)


### Bug Fixes

* ux ([519b66a](https://gitlab.com/appointy/waqt/ui/service2/commit/519b66ae994ed73652729e05721d361807481c89))

### [0.2.78](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.77...v0.2.78) (2020-08-26)


### Bug Fixes

* priority ([08700f3](https://gitlab.com/appointy/waqt/ui/service2/commit/08700f3f14ca17e3acacc95a8cfda4c3e4c3e4b6))

### [0.2.77](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.76...v0.2.77) (2020-08-25)


### Bug Fixes

* pricing autofill ([7a8465a](https://gitlab.com/appointy/waqt/ui/service2/commit/7a8465a36f65fe63e11c3d367a7b55c61d0048b4))

### [0.2.76](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.75...v0.2.76) (2020-08-25)


### Bug Fixes

* show error in wizard ([96697d4](https://gitlab.com/appointy/waqt/ui/service2/commit/96697d4daf82733a20a70279250013fcc7d51502))

### [0.2.75](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.74...v0.2.75) (2020-08-25)


### Features

* priorities in camelcase ([e06ea13](https://gitlab.com/appointy/waqt/ui/service2/commit/e06ea13e830997ad33fab1f3b9baefa651b5937d))
* priorities in spam settings ([5079b21](https://gitlab.com/appointy/waqt/ui/service2/commit/5079b21d4bd026541d2be1728cb7d297abe653a1))


### Bug Fixes

* priorities ([d95ec15](https://gitlab.com/appointy/waqt/ui/service2/commit/d95ec15dac6acf02fd0ab80708eb3fb00495b561))

### [0.2.74](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.73...v0.2.74) (2020-08-24)


### Bug Fixes

* tax can not be more than 100 ([4920d1c](https://gitlab.com/appointy/waqt/ui/service2/commit/4920d1ce1507dd012fcd9103b4b619c3a12342c8))

### [0.2.73](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.72...v0.2.73) (2020-08-22)


### Bug Fixes

* display order ([a1f1328](https://gitlab.com/appointy/waqt/ui/service2/commit/a1f1328c7226ab11afb88f5d0354ba2919a333ab))

### [0.2.72](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.71...v0.2.72) (2020-08-22)


### Features

* display order ([7f5e741](https://gitlab.com/appointy/waqt/ui/service2/commit/7f5e741f13d7450e40a927f943400ce678dfc829))

### [0.2.71](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.70...v0.2.71) (2020-08-22)


### Bug Fixes

* layout props ([a2dec9b](https://gitlab.com/appointy/waqt/ui/service2/commit/a2dec9b22a8549f8c80a019c5583eed4e4dbff95))

### [0.2.70](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.69...v0.2.70) (2020-08-21)


### Bug Fixes

* pricing and durations ([5f8569f](https://gitlab.com/appointy/waqt/ui/service2/commit/5f8569fbcb20f1acc7df012dba8a788febd3ab7a))
* service durations in staff list in service ([4cecf4e](https://gitlab.com/appointy/waqt/ui/service2/commit/4cecf4e0510c8df141b8890476226d90534eaaa9))

### [0.2.69](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.68...v0.2.69) (2020-08-21)


### Features

* handle add and delete duration ([ec23a33](https://gitlab.com/appointy/waqt/ui/service2/commit/ec23a33bcdc0fd886f0861fac8cbdda544ed80c1))
* mutation in service pricing ([6f78b0e](https://gitlab.com/appointy/waqt/ui/service2/commit/6f78b0eb9bd67b309de3f50b756b8bb15bda4e90))
* pricing component ([a359ec7](https://gitlab.com/appointy/waqt/ui/service2/commit/a359ec78fd8260a3d3e59454428262f1d579fbc1))


### Bug Fixes

* pricing ([2cc68e0](https://gitlab.com/appointy/waqt/ui/service2/commit/2cc68e090ee14500d800f7c9dab05197eee5befc))
* pricing ([b3aadee](https://gitlab.com/appointy/waqt/ui/service2/commit/b3aadee58879a9630b99100ed7d33ecff6a1376d))

### [0.2.68](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.67...v0.2.68) (2020-08-21)


### Features

* upsert settings ([7bab579](https://gitlab.com/appointy/waqt/ui/service2/commit/7bab57918023df80585f6e82d1cb274f074e14bc))


### Bug Fixes

* dropdown ([ea974c7](https://gitlab.com/appointy/waqt/ui/service2/commit/ea974c734fe7ce0077c2cf92460be897d1925d02))
* dropdown width ([f31f488](https://gitlab.com/appointy/waqt/ui/service2/commit/f31f488098b122dbb41422bfbb0d2a33ccd7877e))
* duration in linking display ([23ae74d](https://gitlab.com/appointy/waqt/ui/service2/commit/23ae74dbaf7c721366e4e88a70a7d6bd24a1611c))
* files ([38daaf5](https://gitlab.com/appointy/waqt/ui/service2/commit/38daaf526b0093d0608d40f7babb20d7f2d88da1))
* issues ([f0924ae](https://gitlab.com/appointy/waqt/ui/service2/commit/f0924aeb35ead607ab347539d3a0c2577c3a91ae))

### [0.2.67](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.66...v0.2.67) (2020-08-20)


### Bug Fixes

* geographical restrictions ([40fa080](https://gitlab.com/appointy/waqt/ui/service2/commit/40fa080b276f7e86c37e1cf563edbb39675da434))
* staff assigned ([44569ce](https://gitlab.com/appointy/waqt/ui/service2/commit/44569cea798e90070a44e6fbb51ed0f08ccb35ab))

### [0.2.66](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.65...v0.2.66) (2020-08-20)


### Bug Fixes

* layout props ([d49d488](https://gitlab.com/appointy/waqt/ui/service2/commit/d49d4886cf6884fb8455da756ec9737eb521ae9f))

### [0.2.65](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.64...v0.2.65) (2020-08-20)


### Bug Fixes

* layout props ([e9c9265](https://gitlab.com/appointy/waqt/ui/service2/commit/e9c9265ef7e11c97c64a028e8caf0bd4f0e82b7b))

### [0.2.64](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.63...v0.2.64) (2020-08-18)


### Bug Fixes

* relay runtime version ([e12cc00](https://gitlab.com/appointy/waqt/ui/service2/commit/e12cc00c92b26fc64d4a4bdd519514add03446d2))

### [0.2.63](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.62...v0.2.63) (2020-08-18)


### Bug Fixes

* relay update ([3d81e10](https://gitlab.com/appointy/waqt/ui/service2/commit/3d81e102743169238d456ff8b0ab6a1d1b19ad67))

### [0.2.62](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.61...v0.2.62) (2020-08-13)


### Bug Fixes

* service wizard issue ([2ee2e0c](https://gitlab.com/appointy/waqt/ui/service2/commit/2ee2e0cf528ab43a93607d5f65f1d4d03a75f216))

### [0.2.61](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.60...v0.2.61) (2020-08-13)


### Bug Fixes

* alias ([5769edc](https://gitlab.com/appointy/waqt/ui/service2/commit/5769edcb2c5ee7835b270b786fba6361f69f5db9))
* assigned services to employee list ([6bb6d5a](https://gitlab.com/appointy/waqt/ui/service2/commit/6bb6d5ae7fa5f447beb38fc2e5736654731968fb))
* assigned services to employee summary ([5ef8c84](https://gitlab.com/appointy/waqt/ui/service2/commit/5ef8c84051169e0b81fd74a4bd8461ed5bce3c1e))
* assigned services to location list ([ebc8fbb](https://gitlab.com/appointy/waqt/ui/service2/commit/ebc8fbb262acce4a8a51d76a29f7f6ed876fddb4))
* linking ([4b81960](https://gitlab.com/appointy/waqt/ui/service2/commit/4b81960ad94b2dbca5817c212d3136e70ba76549))

### [0.2.60](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.59...v0.2.60) (2020-08-12)


### Bug Fixes

* new line ([662962e](https://gitlab.com/appointy/waqt/ui/service2/commit/662962e794ef91eb44c84ffb40a486bd7b67cf12))

### [0.2.59](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.58...v0.2.59) (2020-08-12)


### Bug Fixes

* new line ([a5e6b01](https://gitlab.com/appointy/waqt/ui/service2/commit/a5e6b011ae6c256371e99ae0693cc555eedbd0ab))

### [0.2.58](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.57...v0.2.58) (2020-08-11)


### Features

* service template assign ([fcb6d7f](https://gitlab.com/appointy/waqt/ui/service2/commit/fcb6d7fcdcabe3c3ce56a606c75152b2634010b3))


### Bug Fixes

* service template assign ([22551f1](https://gitlab.com/appointy/waqt/ui/service2/commit/22551f1bbe385bd01963a8d901c24f1e7295d14d))

### [0.2.57](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.56...v0.2.57) (2020-08-11)


### Bug Fixes

* quantity alias validation ([d29de61](https://gitlab.com/appointy/waqt/ui/service2/commit/d29de616490098d1f4a791812234d7e5298700a9))

### [0.2.56](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.55...v0.2.56) (2020-08-10)


### Bug Fixes

* summary in discount add ([89785ec](https://gitlab.com/appointy/waqt/ui/service2/commit/89785ec99e9bdce1533e59458585f18b327898bc))

### [0.2.55](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.54...v0.2.55) (2020-08-10)


### Bug Fixes

* summary in discount add ([c050703](https://gitlab.com/appointy/waqt/ui/service2/commit/c05070308766790ba3b41cc4871f20b58f485642))

### [0.2.54](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.53...v0.2.54) (2020-08-10)


### Features

* summary in discount add ([4480646](https://gitlab.com/appointy/waqt/ui/service2/commit/4480646839a9105068beb3a8593a0ab35df68c67))

### [0.2.53](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.52...v0.2.53) (2020-08-07)


### Bug Fixes

* discount hook ([d63f556](https://gitlab.com/appointy/waqt/ui/service2/commit/d63f55684ef48d46b9adc3b173d33e4fee7e1970))

### [0.2.52](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.51...v0.2.52) (2020-08-07)


### Bug Fixes

* discount hook ([362a9f2](https://gitlab.com/appointy/waqt/ui/service2/commit/362a9f2c875d42f21b25adc42757b3c34f06bf6d))

### [0.2.51](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.50...v0.2.51) (2020-08-07)


### Bug Fixes

* discount hook ([b7da830](https://gitlab.com/appointy/waqt/ui/service2/commit/b7da830efa89aa746308cec11a39e309771d61e0))

### [0.2.50](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.49...v0.2.50) (2020-08-07)


### Bug Fixes

* discount hook ([d6fcd8a](https://gitlab.com/appointy/waqt/ui/service2/commit/d6fcd8a8dbb27640f1140baa773dda07cb5a325e))

### [0.2.49](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.48...v0.2.49) (2020-08-05)


### Bug Fixes

* discount hook ([d4c96ae](https://gitlab.com/appointy/waqt/ui/service2/commit/d4c96ae95732120a118efee915951945ab97233c))

### [0.2.48](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.47...v0.2.48) (2020-08-05)


### Bug Fixes

* use memo in component ([76d1715](https://gitlab.com/appointy/waqt/ui/service2/commit/76d171514a509d985e4c8b232c7af1ae0e685cdf))

### [0.2.47](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.46...v0.2.47) (2020-08-04)


### Features

* discountable items in add ([36daf11](https://gitlab.com/appointy/waqt/ui/service2/commit/36daf11f7355de679276edf930ba230739cd3ea8))

### [0.2.46](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.44...v0.2.46) (2020-08-01)


### Features

* discountable items ([1e246e4](https://gitlab.com/appointy/waqt/ui/service2/commit/1e246e4198f4f184ddfd933f844474ffd985c17b))

### [0.2.45](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.44...v0.2.45) (2020-08-01)


### Features

* discountable items ([1e246e4](https://gitlab.com/appointy/waqt/ui/service2/commit/1e246e4198f4f184ddfd933f844474ffd985c17b))

### [0.2.44](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.43...v0.2.44) (2020-07-30)


### Bug Fixes

* trans in % ([8faf8e9](https://gitlab.com/appointy/waqt/ui/service2/commit/8faf8e9475c51788be03b89c240767f710d0ea1b))

### [0.2.43](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.42...v0.2.43) (2020-07-30)


### Features

* create service using service template in step1 ([bac70c6](https://gitlab.com/appointy/waqt/ui/service2/commit/bac70c69152698dac7b0aabf2fb0faaf1fa84e0e))
* create service using service template in step5 ([08e5127](https://gitlab.com/appointy/waqt/ui/service2/commit/08e5127e7ce9c33ac7e030d90e13a1b496c6927c))

### [0.2.42](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.41...v0.2.42) (2020-07-30)


### Bug Fixes

* cancellation settings ([d43ecdb](https://gitlab.com/appointy/waqt/ui/service2/commit/d43ecdb39e7686e7337313978f09b36e390471d5))
* code review ([5cfc6cc](https://gitlab.com/appointy/waqt/ui/service2/commit/5cfc6cc9db80487c47aa34b29ea101dd218cc29a))
* payment settings ui ([819212f](https://gitlab.com/appointy/waqt/ui/service2/commit/819212feecb020699ee7c79d18e6668ac07c5a2d))
* tax settings ui ([d3971cf](https://gitlab.com/appointy/waqt/ui/service2/commit/d3971cfa46c2541dc48a9c4770f2b38de77cc3b6))

### [0.2.41](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.40...v0.2.41) (2020-07-29)


### Features

* availability type ([c3c0878](https://gitlab.com/appointy/waqt/ui/service2/commit/c3c0878ab4f2e482fa979d8a02491d853dea1242))


### Bug Fixes

* availability type ([aec655a](https://gitlab.com/appointy/waqt/ui/service2/commit/aec655a85a4f8032a3c46cfbb9964386004ef1cb))

### [0.2.40](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.39...v0.2.40) (2020-07-29)


### Features

* validate precision ([b233f34](https://gitlab.com/appointy/waqt/ui/service2/commit/b233f34d3fb3720832d28687f6f5f1d82b27fbcd))

### [0.2.39](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.38...v0.2.39) (2020-07-29)


### Bug Fixes

* collapse in payment settings ([369d5a7](https://gitlab.com/appointy/waqt/ui/service2/commit/369d5a76f9e094b1d0f74510371d896c346525dc))

### [0.2.38](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.37...v0.2.38) (2020-07-27)


### Features

* accept pre payment ([bf0c26e](https://gitlab.com/appointy/waqt/ui/service2/commit/bf0c26eef3c6a344538130de36a28b5abd282ba0))
* min time interval ([baabd31](https://gitlab.com/appointy/waqt/ui/service2/commit/baabd3138e39ec4f4987c954ece6f5fa5b6e66a2))
* non email verified users ([7ca18eb](https://gitlab.com/appointy/waqt/ui/service2/commit/7ca18eb390cb8b0b72dd583eb4e9e1dafb499dbd))
* time settings ([d8fab27](https://gitlab.com/appointy/waqt/ui/service2/commit/d8fab27ea5455b12e1f426e0f0748176f8702cc7))
* time settings summary ([ed6a342](https://gitlab.com/appointy/waqt/ui/service2/commit/ed6a342c1e9940db2ad17b2f7a8b99ef0fdbf9ee))
* validate precision pattern ([0442b28](https://gitlab.com/appointy/waqt/ui/service2/commit/0442b283bb8ed95df1ec69a33ee16752a723bbee))


### Bug Fixes

* recurring settings ([e9d5afa](https://gitlab.com/appointy/waqt/ui/service2/commit/e9d5afa7fb0c913bc33a13728b02f82db1363698))
* time settings ([69f281b](https://gitlab.com/appointy/waqt/ui/service2/commit/69f281b0856a1cb48fd12075fedf178e12f1d5fe))
* time settings save ([c63a252](https://gitlab.com/appointy/waqt/ui/service2/commit/c63a25227f25c1cd3dc386678b2c0a1c35beca97))

### [0.2.37](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.36...v0.2.37) (2020-07-22)


### Bug Fixes

* refetching after bulk upload ([8d93b92](https://gitlab.com/appointy/waqt/ui/service2/commit/8d93b92405e38f33e539e36b1376f5c682b48a44))

### [0.2.36](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.35...v0.2.36) (2020-07-21)


### Features

* alias in lead and cancellation settings and linking ([62f79d4](https://gitlab.com/appointy/waqt/ui/service2/commit/62f79d4ea8a1a4a84b89a883aee459450bc82303))
* alias in settings ([81c93b4](https://gitlab.com/appointy/waqt/ui/service2/commit/81c93b4ac68073910ad837786770196f00465006))


### Bug Fixes

* alias ([06816c3](https://gitlab.com/appointy/waqt/ui/service2/commit/06816c396725e70753eff2f27593cb32ac9742ff))

### [0.2.35](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.34...v0.2.35) (2020-07-20)


### Bug Fixes

* build issue ([569d924](https://gitlab.com/appointy/waqt/ui/service2/commit/569d924f96c6ad7c4992748e339fb4eae7a1ea78))

### [0.2.34](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.33...v0.2.34) (2020-07-20)


### Features

* alias ([7ea7d96](https://gitlab.com/appointy/waqt/ui/service2/commit/7ea7d9602b5ffcbe9eaaf526b62317f482c36c0e))

### [0.2.33](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.32...v0.2.33) (2020-07-20)


### Bug Fixes

* do not show refund policy in zero pricing service ([5fcc503](https://gitlab.com/appointy/waqt/ui/service2/commit/5fcc5038983e6471c6a3216fa514dd561fe8f19a))

### [0.2.32](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.31...v0.2.32) (2020-07-20)


### Bug Fixes

* do not service linking if employee not assigned to locations ([21b7b28](https://gitlab.com/appointy/waqt/ui/service2/commit/21b7b2838388780fbdce3c3ebc3d85438fda9d4b))

### [0.2.31](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.30...v0.2.31) (2020-07-17)


### Bug Fixes

* show services of locations linked to employee ([1f0d82b](https://gitlab.com/appointy/waqt/ui/service2/commit/1f0d82bdcf3bf075e0c137fdc57ea7e2cee7045f))

### [0.2.30](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.29...v0.2.30) (2020-07-17)


### Features

* **bulkimport:** autofill and sample csv ([75c4275](https://gitlab.com/appointy/waqt/ui/service2/commit/75c4275b3e9562c2cf80a2bf7377f22589127c07))


### Bug Fixes

* removed logs ([17db611](https://gitlab.com/appointy/waqt/ui/service2/commit/17db6113d5d8665fac600ecefeea34aad70e0412))

### [0.2.29](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.28...v0.2.29) (2020-07-16)


### Bug Fixes

* example for domain ([bac0c09](https://gitlab.com/appointy/waqt/ui/service2/commit/bac0c09d8fbb3c7ac690079f306a2ee5009acf75))

### [0.2.28](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.27...v0.2.28) (2020-07-16)


### Bug Fixes

* do not show inactive and staff not providing services in linking ([2b4099c](https://gitlab.com/appointy/waqt/ui/service2/commit/2b4099cd680fc002346684c367ad2b63d3f9733a))

### [0.2.27](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.26...v0.2.27) (2020-07-16)


### Bug Fixes

* service wizard validation ([907e314](https://gitlab.com/appointy/waqt/ui/service2/commit/907e31494e94e62f235fcba531087464b1da50ba))

### [0.2.26](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.25...v0.2.26) (2020-07-15)


### Bug Fixes

* stepper ([2de4ab9](https://gitlab.com/appointy/waqt/ui/service2/commit/2de4ab98f8f1ced7ee2dea84ee3644cf05b6e01d))

### [0.2.25](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.24...v0.2.25) (2020-07-15)


### Bug Fixes

* service pricing in step2 ([0d02d9b](https://gitlab.com/appointy/waqt/ui/service2/commit/0d02d9b4e93b366079358eb8a299ae26648caa10))
* show employees which provides services in service add wizard ([1209d5a](https://gitlab.com/appointy/waqt/ui/service2/commit/1209d5ae1c8f5e23059c64246acda78304169fce))

### [0.2.24](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.23...v0.2.24) (2020-07-14)


### Features

* service details in add form step 2 ([d4e4af9](https://gitlab.com/appointy/waqt/ui/service2/commit/d4e4af96242caae25e1100a00a7eecca79946c7b))


### Bug Fixes

* hide back button and enable previous in other steps ([5aa3556](https://gitlab.com/appointy/waqt/ui/service2/commit/5aa3556a9dbdfbb87d07d232118f163481913cba))

### [0.2.23](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.22...v0.2.23) (2020-07-14)


### Bug Fixes

* fix service type mapping ([be2287e](https://gitlab.com/appointy/waqt/ui/service2/commit/be2287e83cdf974a231ecc2a12263997a3c87c3d))

### [0.2.22](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.21...v0.2.22) (2020-07-14)


### Bug Fixes

* do not allow user to skip service step ([e172cec](https://gitlab.com/appointy/waqt/ui/service2/commit/e172cec613d7a9beea6f0878646ca5ea5dc2121d))

### [0.2.21](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.20...v0.2.21) (2020-07-14)


### Bug Fixes

* do not allow user to skip service step ([824e62a](https://gitlab.com/appointy/waqt/ui/service2/commit/824e62ac20da245fac2008f62df09009b3e0ffa4))

### [0.2.20](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.19...v0.2.20) (2020-07-14)


### Bug Fixes

* do not allow user to skip service step ([a4a0a2b](https://gitlab.com/appointy/waqt/ui/service2/commit/a4a0a2b8ca8a6d3bcb83995483afea40c0549ca0))

### [0.2.19](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.18...v0.2.19) (2020-07-14)


### Bug Fixes

* linking summary ([6fbba65](https://gitlab.com/appointy/waqt/ui/service2/commit/6fbba6575c51a83ab081c885c740daff1461b6b3))

### [0.2.18](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.17...v0.2.18) (2020-07-13)


### Bug Fixes

* service info null service ([8262a5d](https://gitlab.com/appointy/waqt/ui/service2/commit/8262a5da0af3dbd214031408e5d09b1795898d22))

### [0.2.17](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.16...v0.2.17) (2020-07-13)


### Bug Fixes

* fix service policy attach ([51dcd99](https://gitlab.com/appointy/waqt/ui/service2/commit/51dcd995c3e38cfcdb8683c9071cbc8821067449))

### [0.2.16](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.15...v0.2.16) (2020-07-13)


### Bug Fixes

* time settings ([d1f0a23](https://gitlab.com/appointy/waqt/ui/service2/commit/d1f0a23bc794ab789bbc7d6cbc1b31d3d6d1be8b))

### [0.2.15](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.14...v0.2.15) (2020-07-13)


### Bug Fixes
* fix service policy attach ([d47015e](https://gitlab.com/appointy/waqt/ui/service2/commit/d47015ea5280fc9590ceba2cf6a79cfde9b45c4d))

* time settings ([d1f0a23](https://gitlab.com/appointy/waqt/ui/service2/commit/d1f0a23bc794ab789bbc7d6cbc1b31d3d6d1be8b))

### [0.2.14](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.13...v0.2.14) (2020-07-13)


### Features

* **upload:** image remove option ([6a21013](https://gitlab.com/appointy/waqt/ui/service2/commit/6a210133e19956c6a5c24390bd7fb51b31fca04a))


### Bug Fixes

* lead and cancellation policy summary ([ee8d69f](https://gitlab.com/appointy/waqt/ui/service2/commit/ee8d69fecf3ffdaf3ef556a4f10fc51bf2252620))

### [0.2.13](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.12...v0.2.13) (2020-07-13)


### Features

* **upload:** image remove option ([6a21013](https://gitlab.com/appointy/waqt/ui/service2/commit/6a210133e19956c6a5c24390bd7fb51b31fca04a))

### [0.2.12](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.11...v0.2.12) (2020-07-13)

### [0.2.11](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.10...v0.2.11) (2020-07-13)


### Bug Fixes

* remove expanded from booking rules ([f29649b](https://gitlab.com/appointy/waqt/ui/service2/commit/f29649be87ba005918f68febaac4ff14ef68904f))

### [0.2.10](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.9...v0.2.10) (2020-07-11)


### Features

* **bug fix:** package ([00b5d25](https://gitlab.com/appointy/waqt/ui/service2/commit/00b5d2596502c5b12b291a19574a3d3ad5ee1498))

### [0.1.191](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.190...v0.1.191) (2020-07-04)

### [0.1.190](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.189...v0.1.190) (2020-07-03)

### [0.2.9](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.8...v0.2.9) (2020-07-10)


### Bug Fixes

* roles and rights of employee ([682b732](https://gitlab.com/appointy/waqt/ui/service2/commit/682b732d2022e4833f7d43b2f658f75aa571f5b7))

### [0.2.8](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.7...v0.2.8) (2020-07-10)


### Bug Fixes

* add customer restriction ([ce298c1](https://gitlab.com/appointy/waqt/ui/service2/commit/ce298c18cb4ee86514ac5b510733ce203a10eecc))
* advance booking options ([cab4882](https://gitlab.com/appointy/waqt/ui/service2/commit/cab48824f80e01cc96ebff2bd0157c4b0502c78f))
* booking rules refetch ([d379cd3](https://gitlab.com/appointy/waqt/ui/service2/commit/d379cd35a66a20ab2cd85f4abba9ff68cbc32bdb))
* url validation in advance options ([7203170](https://gitlab.com/appointy/waqt/ui/service2/commit/7203170eefe6e4f839a8cf49240cdf20e84c56fe))

### [0.2.7](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.6...v0.2.7) (2020-07-10)


### Bug Fixes

* fix service ux ([647afd7](https://gitlab.com/appointy/waqt/ui/service2/commit/647afd71c635d3ff7221f2363953c132b5cedf7d))

### [0.2.6](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.5...v0.2.6) (2020-07-09)


### Bug Fixes

* release issues ([a483fe5](https://gitlab.com/appointy/waqt/ui/service2/commit/a483fe52c957a84a6fac0234de82d5e5ec9e0065))

### [0.2.5](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.4...v0.2.5) (2020-07-09)


### Bug Fixes

* default image ([08e0c5f](https://gitlab.com/appointy/waqt/ui/service2/commit/08e0c5f9f3c73fa0b480b0cd5c01300494b7dd45))

### [0.2.4](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.3...v0.2.4) (2020-07-09)


### Features

* confirm container on reset settings ([5ab48e1](https://gitlab.com/appointy/waqt/ui/service2/commit/5ab48e1d578dd5dcbfcebe76ceaa2311175ac6d6))


### Bug Fixes

* confirm container on reset settings ([cd56c83](https://gitlab.com/appointy/waqt/ui/service2/commit/cd56c83780b4eedcf63bc4d34be97bc45e369950))

### [0.2.3](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.2...v0.2.3) (2020-07-08)


### Features

* default image ([f36b50b](https://gitlab.com/appointy/waqt/ui/service2/commit/f36b50ba43b11689cbe33237a7d92d33d4005ded))
* default image ([55dfdfd](https://gitlab.com/appointy/waqt/ui/service2/commit/55dfdfd848ef3634d0779dee4c8e2c802358dbfc))

### [0.2.2](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.2.1...v0.2.2) (2020-07-08)


### Bug Fixes

* loading in linked services ([ab6146e](https://gitlab.com/appointy/waqt/ui/service2/commit/ab6146ed5c32b8793dd0ee0873402c35f5bdc0cd))

### [0.2.1](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.197...v0.2.1) (2020-07-08)


### Features

* heading in linked services ([a0c40f1](https://gitlab.com/appointy/waqt/ui/service2/commit/a0c40f1b8fb618dbc8a7438253535169258922b8))


### Bug Fixes

* service pricing in employee ([7f66f54](https://gitlab.com/appointy/waqt/ui/service2/commit/7f66f541fb0bc6ca4dad78619a99db9490f46a2a))

### [0.1.197](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.196...v0.1.197) (2020-07-07)


### Features

* create service from location admin ([9009020](https://gitlab.com/appointy/waqt/ui/service2/commit/900902083935befc62604405272db89749c09fd6))

### [0.1.196](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.195...v0.1.196) (2020-07-07)


### Features

* set booking restriction settings ([f65a67f](https://gitlab.com/appointy/waqt/ui/service2/commit/f65a67f8ab4d6383364e710cfaa4609a683c9f89))
* set default lead and cancellation settings ([e8a0c9e](https://gitlab.com/appointy/waqt/ui/service2/commit/e8a0c9e26cbdfc16ed1dcc5fcaa4583956a345e4))
* set default spam settings ([45fbb54](https://gitlab.com/appointy/waqt/ui/service2/commit/45fbb549bcef054f605899683efe76f1ab608574))
* set payment settings ([9be59a8](https://gitlab.com/appointy/waqt/ui/service2/commit/9be59a8830eaddd0789e53808b095841f849b5d7))

### [0.1.195](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.194...v0.1.195) (2020-07-07)


### Bug Fixes

* service resource linking ([8febb03](https://gitlab.com/appointy/waqt/ui/service2/commit/8febb0352d9c291cbade12ef8e8d5f7c7fe70cef))

### [0.1.194](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.193...v0.1.194) (2020-07-07)


### Bug Fixes

* service resource linking ([905da5f](https://gitlab.com/appointy/waqt/ui/service2/commit/905da5f52b19d7e96ffff26d0b9a92b155e89148))

### [0.1.193](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.192...v0.1.193) (2020-07-07)


### Features

* service resource linking ([34a31f0](https://gitlab.com/appointy/waqt/ui/service2/commit/34a31f0dcdfe0fbcaace0097bc1b89064226fbe8))

### [0.1.192](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.191...v0.1.192) (2020-07-06)

### [0.1.191](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.190...v0.1.191) (2020-07-04)


### Bug Fixes

* fix settings ([605f18e](https://gitlab.com/appointy/waqt/ui/service2/commit/605f18e59fe01624130ecb48f5ed91d9322f96dd))

### [0.1.190](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.189...v0.1.190) (2020-07-04)


### Bug Fixes

* fix settings ([acc2dfd](https://gitlab.com/appointy/waqt/ui/service2/commit/acc2dfdfaa21a356661e41caa55dc9a851849746))
* minor changes ([b8150aa](https://gitlab.com/appointy/waqt/ui/service2/commit/b8150aa4d4a04e72b165bd779a71cb3e35ea8bde))

### [0.1.189](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.188...v0.1.189) (2020-07-03)


### Bug Fixes

* module roles in service ([a9148f1](https://gitlab.com/appointy/waqt/ui/service2/commit/a9148f12cc729aef5bf726bf3b2295d7944f0a2f))
* module roles in settings ([ed92d69](https://gitlab.com/appointy/waqt/ui/service2/commit/ed92d6998d9a870ad6a51115027d02c9f631b41e))
* module roles in settings ([a19384b](https://gitlab.com/appointy/waqt/ui/service2/commit/a19384b8f41262fcf456e437e490bba59fe01bfe))
* service employee linking module roles ([9ed9484](https://gitlab.com/appointy/waqt/ui/service2/commit/9ed948446ff604d5656bc8de8399cd2a40df34d9))
* settings summary font size ([330c2a1](https://gitlab.com/appointy/waqt/ui/service2/commit/330c2a16befa55614477b2afc83e251d0fc9638c))

### [0.1.188](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.187...v0.1.188) (2020-07-03)


### Bug Fixes

* settings old code removed ([3bc1760](https://gitlab.com/appointy/waqt/ui/service2/commit/3bc17607abe51de49800a0fc85a5aafec1f0f9e8))

### [0.1.187](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.186...v0.1.187) (2020-07-03)


### Bug Fixes

* first image as default image ([6fcac9c](https://gitlab.com/appointy/waqt/ui/service2/commit/6fcac9c6ef4b5e0fe82671d8eeb9fc2d57bf5bbd))
* hide payment settings in case of zero pricing service ([cb92089](https://gitlab.com/appointy/waqt/ui/service2/commit/cb92089968c75ca2b30e06f6d1e39ffcbc56bc8c))

### [0.1.186](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.185...v0.1.186) (2020-07-03)


### Bug Fixes

* category shown as default when service is created ([b775e1e](https://gitlab.com/appointy/waqt/ui/service2/commit/b775e1ef9cca9c9ba65e909a989f6ded6a97b56c))

### [0.1.185](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.184...v0.1.185) (2020-07-03)


### Bug Fixes

* loading in service employee linking ([498fdec](https://gitlab.com/appointy/waqt/ui/service2/commit/498fdec3eb4e8fdb1be19999e79f9102f35f76b3))

### [0.1.184](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.183...v0.1.184) (2020-07-03)


### Bug Fixes

* remove old booking rules ([57b0764](https://gitlab.com/appointy/waqt/ui/service2/commit/57b07648eb0fe621d94dff287bc238ea7798af94))

### [0.1.183](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.182...v0.1.183) (2020-07-03)


### Features

* advance options ([3cfe87f](https://gitlab.com/appointy/waqt/ui/service2/commit/3cfe87f19c57c8c40edbb2b3399d4e405fc7d086))
* advance options summary ([aed1e21](https://gitlab.com/appointy/waqt/ui/service2/commit/aed1e2128b0ddabb64e337b99c29fcee914e629e))

### [0.1.182](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.181...v0.1.182) (2020-07-03)


### Bug Fixes

* bulkimport validations ([2a9eb54](https://gitlab.com/appointy/waqt/ui/service2/commit/2a9eb54e586ce875516f1400744e5f97821c22c9))

### [0.1.181](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.180...v0.1.181) (2020-07-02)


### Features

* booking rules ([c3e24d8](https://gitlab.com/appointy/waqt/ui/service2/commit/c3e24d80c9f0b9f4340ecc0cc181bf2438eec565))

### [0.1.180](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.179...v0.1.180) (2020-07-02)


### Bug Fixes

* booking restrictions booking limits ([36cb8c0](https://gitlab.com/appointy/waqt/ui/service2/commit/36cb8c03b7f2939bc841c29457a29877854bf61c))
* booking restrictions summary ([391d6ac](https://gitlab.com/appointy/waqt/ui/service2/commit/391d6ac116f15161c544bdf518866bbecd2d9577))

### [0.1.179](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.178...v0.1.179) (2020-07-02)


### Bug Fixes

* booking restrictions ([374daee](https://gitlab.com/appointy/waqt/ui/service2/commit/374daee6afcd726d381910848b8a40c2ca02a34b))
* payment settings ([97c5639](https://gitlab.com/appointy/waqt/ui/service2/commit/97c56398a185db1940e0a7ad34c7b2d1ff19fb79))
* summary font size ([7951bd9](https://gitlab.com/appointy/waqt/ui/service2/commit/7951bd97556b2b9b38ec19a21345911436bd0cab))

### [0.1.178](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.177...v0.1.178) (2020-07-02)


### Features

* lead and cancellation settings, update not working ([8b3c5e5](https://gitlab.com/appointy/waqt/ui/service2/commit/8b3c5e5e1505173ddc72ea7ee02515028e33eac0))

### [0.1.177](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.176...v0.1.177) (2020-07-02)


### Features

* policy acceptance new ux ([876710e](https://gitlab.com/appointy/waqt/ui/service2/commit/876710e4535e3c3695baa5366d0d5d7c8dd1b527))
* spam settings ([9d23275](https://gitlab.com/appointy/waqt/ui/service2/commit/9d23275ef1f4d8340ff48c792a2815b61cdccc8a))
* spam settings ([394710e](https://gitlab.com/appointy/waqt/ui/service2/commit/394710e3af237fb3c8bc787288b2f9161f359de1))
* styles.ts ([c6224b2](https://gitlab.com/appointy/waqt/ui/service2/commit/c6224b290caa663bcadffeba69adabd8a8d2635e))
* time settings ([f9260c0](https://gitlab.com/appointy/waqt/ui/service2/commit/f9260c0b3ce7ee6ea5953bc24619e4bee948910e))

### [0.1.176](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.175...v0.1.176) (2020-07-01)


### Bug Fixes

* release issues ([af65c81](https://gitlab.com/appointy/waqt/ui/service2/commit/af65c810bff77fc7c5025b339bc4eb917999d131))

### [0.1.175](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.174...v0.1.175) (2020-07-01)


### Features

* service settings - basic settings ([30ce993](https://gitlab.com/appointy/waqt/ui/service2/commit/30ce9930ad14b540fececf39f2173781120f1295))
* service settings - basic settings with summary ([708a61d](https://gitlab.com/appointy/waqt/ui/service2/commit/708a61deb998358c7da6eab160ab8fed4a32d4d5))


### Bug Fixes

* removed extra code ([583dcd7](https://gitlab.com/appointy/waqt/ui/service2/commit/583dcd71a6101f0069e51807cd8dfd101b313704))

### [0.1.174](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.173...v0.1.174) (2020-07-01)


### Bug Fixes

* fix currency trans ([60738de](https://gitlab.com/appointy/waqt/ui/service2/commit/60738de1c28afc1689b60f9c6314efca26520134))

### [0.1.173](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.172...v0.1.173) (2020-07-01)


### Bug Fixes

* fix currency trans ([0434109](https://gitlab.com/appointy/waqt/ui/service2/commit/043410921b2ed9610d288c7372246143c78124be))

### [0.1.172](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.171...v0.1.172) (2020-07-01)


### Bug Fixes

* package versions ([42142a8](https://gitlab.com/appointy/waqt/ui/service2/commit/42142a8a9b095df1c267a0c08377f69c5e30c545))

### [0.1.171](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.170...v0.1.171) (2020-06-30)


### Bug Fixes

* linking in expansion card ([5f595f9](https://gitlab.com/appointy/waqt/ui/service2/commit/5f595f97838a2de85076e79a77758d8076e79fbf))

### [0.1.170](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.169...v0.1.170) (2020-06-30)


### Bug Fixes

* linking in expansion card ([fad6728](https://gitlab.com/appointy/waqt/ui/service2/commit/fad67287aaf5ef6d66b9c20d01738163692fb63f))

### [0.1.169](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.168...v0.1.169) (2020-06-30)


### Bug Fixes

* linking in expansion card ([3e932d4](https://gitlab.com/appointy/waqt/ui/service2/commit/3e932d4280d65d95eaa4a1cedf96d5cdb0d0e606))
* linking in expansion card ([cddfc58](https://gitlab.com/appointy/waqt/ui/service2/commit/cddfc58cd530a10582780cb1dfa8854e908cecbf))

### [0.1.168](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.167...v0.1.168) (2020-06-30)


### Bug Fixes

* divider ([82d8db7](https://gitlab.com/appointy/waqt/ui/service2/commit/82d8db78132c57658cd30d1308f7367dea4dec0b))

### [0.1.167](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.166...v0.1.167) (2020-06-30)


### Bug Fixes

* assign button message ([0719fde](https://gitlab.com/appointy/waqt/ui/service2/commit/0719fdeff6b80c185b5c8ad08bac63c03aee46f6))

### [0.1.166](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.165...v0.1.166) (2020-06-30)


### Features

* assign employee to services in expansion card ([e396888](https://gitlab.com/appointy/waqt/ui/service2/commit/e39688837da77751c9a7b5c8bb266b813e950a24))

### [0.1.165](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.164...v0.1.165) (2020-06-30)


### Bug Fixes

* routes issue due to bulk import ([631c003](https://gitlab.com/appointy/waqt/ui/service2/commit/631c003a378b7c7c606d328b51b2a07d4db10f77))

### [0.1.164](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.163...v0.1.164) (2020-06-29)


### Features

* bulk import added ([0feda65](https://gitlab.com/appointy/waqt/ui/service2/commit/0feda65e598eeed80d5b89df1d486d55d1c79c53))

### [0.1.163](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.162...v0.1.163) (2020-06-27)


### Features

* employee roles ([c92c032](https://gitlab.com/appointy/waqt/ui/service2/commit/c92c032e1987bcc8459a81ac13f1f09eaa3bb494))

### [0.1.162](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.161...v0.1.162) (2020-06-26)


### Bug Fixes

* icon click ([c07406e](https://gitlab.com/appointy/waqt/ui/service2/commit/c07406e96918bceddbaf4fd079e8017c4260ec92))

### [0.1.161](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.160...v0.1.161) (2020-06-26)


### Bug Fixes

* service hooks ([948e444](https://gitlab.com/appointy/waqt/ui/service2/commit/948e4449668184751b64e42ca553359bcfdafaf1))

### [0.1.160](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.159...v0.1.160) (2020-06-26)


### Features

* basic settings and time settings ([c029c85](https://gitlab.com/appointy/waqt/ui/service2/commit/c029c854b0cdc141ed82ffd3b81cf4cf1fa797b4))
* bookings per day ([3d853fb](https://gitlab.com/appointy/waqt/ui/service2/commit/3d853fb676c7e2beaadb7e00db740cc1a8e55a2b))
* policy hook ([c4aaa6d](https://gitlab.com/appointy/waqt/ui/service2/commit/c4aaa6d41f068cd441d052ddd5f11c4872e8a187))

### [0.1.159](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.158...v0.1.159) (2020-06-26)


### Bug Fixes

* assign link button fix ([eb971c6](https://gitlab.com/appointy/waqt/ui/service2/commit/eb971c66e40b96b8c89f76012e134a9855db9a6b))

### [0.1.158](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.157...v0.1.158) (2020-06-25)


### Bug Fixes

* no staff ([071b820](https://gitlab.com/appointy/waqt/ui/service2/commit/071b820a752eee19c12c5111f68f17e3cbd71b05))

### [0.1.157](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.156...v0.1.157) (2020-06-25)


### Bug Fixes

* no employees ([9a6e9c9](https://gitlab.com/appointy/waqt/ui/service2/commit/9a6e9c9688eb5751595e9bbf10c3b4dfc0a5f1ef))

### [0.1.156](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.155...v0.1.156) (2020-06-24)


### Features

* employee service linking actions ([86ab8ba](https://gitlab.com/appointy/waqt/ui/service2/commit/86ab8ba281cd4abf90fd7c5f0ee49aee73a551c8))
* employee service linking changed ([31811f2](https://gitlab.com/appointy/waqt/ui/service2/commit/31811f27a04db40fbd230095ba6a66656fc6593e))

### [0.1.155](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.154...v0.1.155) (2020-06-19)


### Features

* employee service linking provide services ([30f6325](https://gitlab.com/appointy/waqt/ui/service2/commit/30f6325e26edbd2b9fc83c570cbe9839a94d1ddf))

### [0.1.154](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.153...v0.1.154) (2020-06-19)


### Features

* employee service linking ([b9ceb33](https://gitlab.com/appointy/waqt/ui/service2/commit/b9ceb33527f2d491309aa6cb1f0e32e741806a4f))


### Bug Fixes

* versions ([7f4df53](https://gitlab.com/appointy/waqt/ui/service2/commit/7f4df53d6aecde805773670b5eb996536ffda195))

### [0.1.153](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.152...v0.1.153) (2020-06-18)


### Bug Fixes

* wq-583 ([2c9ae52](https://gitlab.com/appointy/waqt/ui/service2/commit/2c9ae52757cd28d0d50fe670e5fc781123be1af9))

### [0.1.152](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.151...v0.1.152) (2020-06-17)


### Bug Fixes

* category in create mutation ([0dddaeb](https://gitlab.com/appointy/waqt/ui/service2/commit/0dddaeb0bd3683dfab2ca3dbf1d9bf4ece7f4b48))

### [0.1.151](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.150...v0.1.151) (2020-06-09)


### Bug Fixes

* duration validation ([d506025](https://gitlab.com/appointy/waqt/ui/service2/commit/d5060254cedecefd480162332c9783b1f8868dc4))

### [0.1.150](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.149...v0.1.150) (2020-06-06)


### Bug Fixes

* remove notification settings section ([41bbe22](https://gitlab.com/appointy/waqt/ui/service2/commit/41bbe2289a85cbd528e78aba77b147a9da7bce6c))

### [0.1.149](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.148...v0.1.149) (2020-06-06)


### Bug Fixes

* fixed service performance ([3cb179c](https://gitlab.com/appointy/waqt/ui/service2/commit/3cb179ce4eb3bba0008dab8f5f6fe25596fbef3a))

### [0.1.148](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.147...v0.1.148) (2020-06-06)


### Bug Fixes

* fixed service performance ([f2b86a3](https://gitlab.com/appointy/waqt/ui/service2/commit/f2b86a37e55724095e1835322d5e59516cfaedff))

### [0.1.147](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.146...v0.1.147) (2020-06-06)


### Bug Fixes

* fixed service performance ([730d48e](https://gitlab.com/appointy/waqt/ui/service2/commit/730d48e1b8f3383efb1819fe401ec5b3871635f1))

### [0.1.146](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.145...v0.1.146) (2020-06-05)


### Bug Fixes

* booking rules: allowed domain blur behaviour (wq-580) ([30f142f](https://gitlab.com/appointy/waqt/ui/service2/commit/30f142fdb83b9ebf55c4f491de3e47884f0a517a))
* booking rules: booking limit per customer (wq-579) ([6d67d88](https://gitlab.com/appointy/waqt/ui/service2/commit/6d67d883a127f65dccf00f174b339ac56b9d7047))
* booking rules: change mask only at service level ([73501d9](https://gitlab.com/appointy/waqt/ui/service2/commit/73501d90d264083305720c34a2e5118d5e742c41))
* booking rules: country code save fix (wq-578) ([f281ff9](https://gitlab.com/appointy/waqt/ui/service2/commit/f281ff9df3f676db368a6eee783fa372ee7b88ef))
* booking rules: customer booking restrictions non editable ui ([378b4b9](https://gitlab.com/appointy/waqt/ui/service2/commit/378b4b91b23eb3cd9d8543d9e638d1037a64c7c6))

### [0.1.145](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.144...v0.1.145) (2020-06-04)


### Bug Fixes

* booking rules: reset quantity alias not working (wq-600) ([9d41c64](https://gitlab.com/appointy/waqt/ui/service2/commit/9d41c64a06bc22aa3194c8d1f3c12d2b1b27482d))

### [0.1.144](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.143...v0.1.144) (2020-06-04)


### Bug Fixes

* remove staff steps ([0f99bb0](https://gitlab.com/appointy/waqt/ui/service2/commit/0f99bb025772d02bac9c2a17bd6911997f6668a4))

### [0.1.143](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.142...v0.1.143) (2020-06-03)


### Bug Fixes

* staff add priority ([0be1dd4](https://gitlab.com/appointy/waqt/ui/service2/commit/0be1dd40f96f5918a894e7521fe8000add3cabe1))

### [0.1.142](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.141...v0.1.142) (2020-06-03)


### Features

* service assign to employee in expansion panel ([b1ee546](https://gitlab.com/appointy/waqt/ui/service2/commit/b1ee5461d875297dd9302976e6c653b3c6e39f19))


### Bug Fixes

* service assign ([107d877](https://gitlab.com/appointy/waqt/ui/service2/commit/107d877706f851ed11db1e9b709e0ad3e8707a5f))

### [0.1.141](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.140...v0.1.141) (2020-06-02)

### [0.1.140](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.139...v0.1.140) (2020-06-02)


### Features

* service assign and schedule ([4ca6a66](https://gitlab.com/appointy/waqt/ui/service2/commit/4ca6a6644215f41073ff3b109d1bd5db1b5d650a))
* service employee link delete ([6f81ff0](https://gitlab.com/appointy/waqt/ui/service2/commit/6f81ff0943bd5f254a1dcdca3d8208beaa755edd))

### [0.1.139](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.138...v0.1.139) (2020-06-01)


### Features

* show open hours ([351efe2](https://gitlab.com/appointy/waqt/ui/service2/commit/351efe2639288f0b6d4c9ab1d0457579e544ebcc))
* show open hours link ([ef6865c](https://gitlab.com/appointy/waqt/ui/service2/commit/ef6865cd718ffa17c1895af984ddfb07abccc916))

### [0.1.138](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.137...v0.1.138) (2020-06-01)


### Bug Fixes

* table in assign service to employee ([b9a6ed6](https://gitlab.com/appointy/waqt/ui/service2/commit/b9a6ed6c649a96a472631e889b0a10dc5c3d9b9b))

### [0.1.137](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.136...v0.1.137) (2020-06-01)


### Features

* staff setups ([b86b56d](https://gitlab.com/appointy/waqt/ui/service2/commit/b86b56dd09c41f80cd896ecd90ce5d5f04099d5e))

### [0.1.136](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.135...v0.1.136) (2020-05-30)


### Bug Fixes

* fixed category dropdown in service ([68afe14](https://gitlab.com/appointy/waqt/ui/service2/commit/68afe1401c49a7970860e512625d25d90680c311))
* fixed category dropdown in service ([0229bce](https://gitlab.com/appointy/waqt/ui/service2/commit/0229bce31c7b7bf95a7bf584371cee29081a4707))
* fixed category dropdown in service ([292c79a](https://gitlab.com/appointy/waqt/ui/service2/commit/292c79a256ac5fea95554c3e41c8a053d98d7a4f))

### [0.1.135](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.132...v0.1.135) (2020-05-30)


### Bug Fixes

* fixed category dropdown in service ([68afe14](https://gitlab.com/appointy/waqt/ui/service2/commit/68afe1401c49a7970860e512625d25d90680c311))

### [0.1.134](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.133...v0.1.134) (2020-05-30)


### Features

* advance booking metadata ([52df05b](https://gitlab.com/appointy/waqt/ui/service2/commit/52df05b36a841458667a289660a0006fa9271cb5))
* cancellation and refund policy ([7a0d6e8](https://gitlab.com/appointy/waqt/ui/service2/commit/7a0d6e82daca83f4a0a1facbfe975e65d0f0e359))


### Bug Fixes

* advance booking options ([134cb7c](https://gitlab.com/appointy/waqt/ui/service2/commit/134cb7cd08a72bcea2b63b9a9694df5a6ade83fa))

### [0.1.133](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.130...v0.1.133) (2020-05-30)


### Features

* in progress ([d2194d5](https://gitlab.com/appointy/waqt/ui/service2/commit/d2194d5e5595a1cf423300bc943de55818c890ab))
* notification reminder ([dec9702](https://gitlab.com/appointy/waqt/ui/service2/commit/dec97024365b46ae2e0b4d5e93296265a152d5ca))
* notification settings basic view ([467cd38](https://gitlab.com/appointy/waqt/ui/service2/commit/467cd38897f5f8a03ed8a5ee93ab6d82806ca756))
* notification settings skeleton ([bf85dfb](https://gitlab.com/appointy/waqt/ui/service2/commit/bf85dfb563d051d44540b1ececab3a9225850df0))
* notification settings switch and checkbox working ([9deea88](https://gitlab.com/appointy/waqt/ui/service2/commit/9deea8848ea8ae97b08237605b5cffab11a098a1))
* notification settings switch working ([6a4ad14](https://gitlab.com/appointy/waqt/ui/service2/commit/6a4ad14aaadab41757269d69ec9748de43e74c8f))

### [0.1.134](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.133...v0.1.134) (2020-05-30)


### Features

* advance booking metadata ([52df05b](https://gitlab.com/appointy/waqt/ui/service2/commit/52df05b36a841458667a289660a0006fa9271cb5))
* cancellation and refund policy ([7a0d6e8](https://gitlab.com/appointy/waqt/ui/service2/commit/7a0d6e82daca83f4a0a1facbfe975e65d0f0e359))


### Bug Fixes

* advance booking options ([134cb7c](https://gitlab.com/appointy/waqt/ui/service2/commit/134cb7cd08a72bcea2b63b9a9694df5a6ade83fa))

### [0.1.133](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.132...v0.1.133) (2020-05-30)


### Features

* notification reminder ([dec9702](https://gitlab.com/appointy/waqt/ui/service2/commit/dec97024365b46ae2e0b4d5e93296265a152d5ca))

### [0.1.132](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.131...v0.1.132) (2020-05-29)

### [0.1.131](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.130...v0.1.131) (2020-05-29)


### Features

* in progress ([d2194d5](https://gitlab.com/appointy/waqt/ui/service2/commit/d2194d5e5595a1cf423300bc943de55818c890ab))
* notification settings basic view ([467cd38](https://gitlab.com/appointy/waqt/ui/service2/commit/467cd38897f5f8a03ed8a5ee93ab6d82806ca756))
* notification settings skeleton ([bf85dfb](https://gitlab.com/appointy/waqt/ui/service2/commit/bf85dfb563d051d44540b1ececab3a9225850df0))
* notification settings switch and checkbox working ([9deea88](https://gitlab.com/appointy/waqt/ui/service2/commit/9deea8848ea8ae97b08237605b5cffab11a098a1))
* notification settings switch working ([6a4ad14](https://gitlab.com/appointy/waqt/ui/service2/commit/6a4ad14aaadab41757269d69ec9748de43e74c8f))

### [0.1.130](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.129...v0.1.130) (2020-05-29)

### [0.1.129](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.128...v0.1.129) (2020-05-29)


### Bug Fixes

* fixed category dropdown ([917146b](https://gitlab.com/appointy/waqt/ui/service2/commit/917146b228dbd53db70f0508036cda9f098cf1d8))

### [0.1.128](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.127...v0.1.128) (2020-05-29)


### Bug Fixes

* fixed services ux ([c3eead6](https://gitlab.com/appointy/waqt/ui/service2/commit/c3eead6c3dcc6fe3e5c7900072345567f14c0d2f))

### [0.1.127](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.126...v0.1.127) (2020-05-29)

### [0.1.126](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.125...v0.1.126) (2020-05-29)


### Bug Fixes

* fixed services ux ([d990824](https://gitlab.com/appointy/waqt/ui/service2/commit/d990824f181937cf49f63a69b5a4d12e573cb84b))

### [0.1.125](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.124...v0.1.125) (2020-05-29)


### Bug Fixes

* fixed services ux ([a7309cc](https://gitlab.com/appointy/waqt/ui/service2/commit/a7309cc26bed5b697799c978551fa34a600db341))

### [0.1.124](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.123...v0.1.124) (2020-05-29)


### Bug Fixes

* pages ([bee60b9](https://gitlab.com/appointy/waqt/ui/service2/commit/bee60b97d53ec9c4d001a4db54230ff7e43d68f0))
* schema updates ([30889a2](https://gitlab.com/appointy/waqt/ui/service2/commit/30889a2ef4b379c1baddaaad0f234c47fcfd9f60))
* schema updates ([fa70c8e](https://gitlab.com/appointy/waqt/ui/service2/commit/fa70c8e2ea1a4b4d88166709a8431fce02584844))

### [0.1.123](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.122...v0.1.123) (2020-05-29)


### Bug Fixes

* spam options at service level ([c13da70](https://gitlab.com/appointy/waqt/ui/service2/commit/c13da70d4e258acb5e4ac09e99703e9758d7ec50))

### [0.1.122](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.121...v0.1.122) (2020-05-27)


### Features

* appointment booking flow settings ([dee33e1](https://gitlab.com/appointy/waqt/ui/service2/commit/dee33e18668ce3f9f5859c574d0864d4f6d90945))
* recurring options in appointment booking flow settings ([ef420f1](https://gitlab.com/appointy/waqt/ui/service2/commit/ef420f1c622d7ef48f15c016a4436a814928e6f5))

### [0.1.121](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.120...v0.1.121) (2020-05-23)


### Bug Fixes

* employee skip step ([f7339cb](https://gitlab.com/appointy/waqt/ui/service2/commit/f7339cbfb148f095484768e81fa434d11d0e7dc7))

### [0.1.120](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.119...v0.1.120) (2020-05-16)


### Bug Fixes

* services wizard pricing and linking fix ([9341dff](https://gitlab.com/appointy/waqt/ui/service2/commit/9341dff89598a041dec57d246767664a57245904))

### [0.1.119](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.111...v0.1.119) (2020-05-16)


### Bug Fixes

* priority ([0a2f7bd](https://gitlab.com/appointy/waqt/ui/service2/commit/0a2f7bd29a79635bb42a61df920219f6ef16a88c))

### [0.1.118](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.117...v0.1.118) (2020-05-14)


### Bug Fixes

* roles in add action ([4e8bea4](https://gitlab.com/appointy/waqt/ui/service2/commit/4e8bea4976c13e2af886e05d7f54b46fec5a5c1e))

### [0.1.117](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.116...v0.1.117) (2020-05-14)


### Bug Fixes

* add action ([7ec3032](https://gitlab.com/appointy/waqt/ui/service2/commit/7ec303260a89ad41f78e416f4106c2737ec9e9c5))

### [0.1.116](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.115...v0.1.116) (2020-05-13)


### Bug Fixes

* navigate back on exit in add action ([6f6d698](https://gitlab.com/appointy/waqt/ui/service2/commit/6f6d6985afa61d1e3e5b7e616a9521c1dd540192))

### [0.1.115](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.114...v0.1.115) (2020-05-13)


### Bug Fixes

* layout in add action ([3bb5caf](https://gitlab.com/appointy/waqt/ui/service2/commit/3bb5cafbe5c7013432b4b5811e93f493d6e5c773))

### [0.1.114](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.113...v0.1.114) (2020-05-13)

### [0.1.113](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.112...v0.1.113) (2020-05-13)


### Bug Fixes

* service add action ([26d4972](https://gitlab.com/appointy/waqt/ui/service2/commit/26d4972434deed305cba3640e225e51c71e5e925))

### [0.1.112](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.110...v0.1.112) (2020-05-13)


### Features

* add action ([9e52df7](https://gitlab.com/appointy/waqt/ui/service2/commit/9e52df77685d0ad17008f695bb2928d567f6288f))


### Bug Fixes

* build issue ([d18b7ca](https://gitlab.com/appointy/waqt/ui/service2/commit/d18b7ca5c82a80f1e0256c1e8086a72fa1e1300f))

### [0.1.118](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.117...v0.1.118) (2020-05-14)


### Bug Fixes

* roles in add action ([4e8bea4](https://gitlab.com/appointy/waqt/ui/service2/commit/4e8bea4976c13e2af886e05d7f54b46fec5a5c1e))

### [0.1.117](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.116...v0.1.117) (2020-05-14)


### Bug Fixes

* add action ([7ec3032](https://gitlab.com/appointy/waqt/ui/service2/commit/7ec303260a89ad41f78e416f4106c2737ec9e9c5))

### [0.1.116](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.115...v0.1.116) (2020-05-13)


### Bug Fixes

* navigate back on exit in add action ([6f6d698](https://gitlab.com/appointy/waqt/ui/service2/commit/6f6d6985afa61d1e3e5b7e616a9521c1dd540192))

### [0.1.115](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.114...v0.1.115) (2020-05-13)


### Bug Fixes

* layout in add action ([3bb5caf](https://gitlab.com/appointy/waqt/ui/service2/commit/3bb5cafbe5c7013432b4b5811e93f493d6e5c773))

### [0.1.114](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.113...v0.1.114) (2020-05-13)

### [0.1.113](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.112...v0.1.113) (2020-05-13)


### Bug Fixes

* service add action ([26d4972](https://gitlab.com/appointy/waqt/ui/service2/commit/26d4972434deed305cba3640e225e51c71e5e925))

### [0.1.112](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.111...v0.1.112) (2020-05-13)


### Bug Fixes

* build issue ([d18b7ca](https://gitlab.com/appointy/waqt/ui/service2/commit/d18b7ca5c82a80f1e0256c1e8086a72fa1e1300f))

### [0.1.111](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.110...v0.1.111) (2020-05-13)


### Features

* add action ([9e52df7](https://gitlab.com/appointy/waqt/ui/service2/commit/9e52df77685d0ad17008f695bb2928d567f6288f))

### [0.1.110](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.109...v0.1.110) (2020-05-12)


### Bug Fixes

* parent ([fea50d4](https://gitlab.com/appointy/waqt/ui/service2/commit/fea50d42e9a37bcf3a5f4892030878ba522f5ea6))

### [0.1.109](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.106...v0.1.109) (2020-05-12)


### Bug Fixes

* onclose optional in add ([ddae041](https://gitlab.com/appointy/waqt/ui/service2/commit/ddae04155dd0772f0ff1461ac04a9d6f9496403b))
* parent ([09e5fcc](https://gitlab.com/appointy/waqt/ui/service2/commit/09e5fcc3f67fd3a8be8332fc91a9745c6d626825))
* register hook to addaction ([b499bb2](https://gitlab.com/appointy/waqt/ui/service2/commit/b499bb27b65edce8b13ba19b82a42bddd33db0bc))

### [0.1.108](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.107...v0.1.108) (2020-05-12)

### [0.1.107](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.106...v0.1.107) (2020-05-12)


### Bug Fixes

* register hook to addaction ([b499bb2](https://gitlab.com/appointy/waqt/ui/service2/commit/b499bb27b65edce8b13ba19b82a42bddd33db0bc))

### [0.1.106](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.105...v0.1.106) (2020-05-09)


### Bug Fixes

* assign service to resource ([d446e20](https://gitlab.com/appointy/waqt/ui/service2/commit/d446e20e62db20152dd20f21f8343603e6237bd1))
* assign service to resource ([c2e0c88](https://gitlab.com/appointy/waqt/ui/service2/commit/c2e0c881497ac3932919940f20766ac4ff0a662a))
* assign service to resource type ([23c0f7a](https://gitlab.com/appointy/waqt/ui/service2/commit/23c0f7ad3c2a10ce2b48ee456e91061aeababc8f))
* service employee linking ([75ba2df](https://gitlab.com/appointy/waqt/ui/service2/commit/75ba2df24ddc7f8ab294255f026d960a7efa1d1f))

### [0.1.105](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.104...v0.1.105) (2020-05-08)


### Bug Fixes

* service price update ([7e3e6dc](https://gitlab.com/appointy/waqt/ui/service2/commit/7e3e6dc9c29195615015dfe7f923e27a580c42bc))

### [0.1.104](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.103...v0.1.104) (2020-05-08)


### Bug Fixes

* services wizard pricing and linking fix ([37b1544](https://gitlab.com/appointy/waqt/ui/service2/commit/37b1544c7a552194f61a90120968493e7bf205e3))

### [0.1.103](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.102...v0.1.103) (2020-05-08)

### [0.1.102](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.101...v0.1.102) (2020-05-08)


### Bug Fixes

* services wizard pricing and linking fix ([b1076b4](https://gitlab.com/appointy/waqt/ui/service2/commit/b1076b447dacbecb5b0154580a2eed410cf588b4))

### [0.1.101](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.100...v0.1.101) (2020-05-07)


### Features

* advance options ui ([20eafc7](https://gitlab.com/appointy/waqt/ui/service2/commit/20eafc786fa6bb5081dffb609a4a8991ffc7ed50))
* appointment settings mutation ([1cba3ad](https://gitlab.com/appointy/waqt/ui/service2/commit/1cba3adb7220be702f7843f3617a83ef886488d3))


### Bug Fixes

* geographical restrictions ([521d351](https://gitlab.com/appointy/waqt/ui/service2/commit/521d3510ba56acaf24cad4b1e2b674eb648ce608))

### [0.1.100](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.99...v0.1.100) (2020-05-07)


### Features

* cancellation and refund policy ([725efea](https://gitlab.com/appointy/waqt/ui/service2/commit/725efea40134cbd68a7592524b97b0013bdcf1db))
* geographical restrictions ([f88799c](https://gitlab.com/appointy/waqt/ui/service2/commit/f88799c40489491e1c7caf09ef9e830cf221a11b))

### [0.1.99](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.98...v0.1.99) (2020-05-04)


### Bug Fixes

* service wizard skip call on step skip ([d0b2a62](https://gitlab.com/appointy/waqt/ui/service2/commit/d0b2a62458540ce2668d5c7c27f2ab51c5c4e892))

### [0.1.98](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.97...v0.1.98) (2020-05-04)


### Bug Fixes

* tax settings add ([6c3024d](https://gitlab.com/appointy/waqt/ui/service2/commit/6c3024dd6f5013631e78022cd1e00d8e86254576))

### [0.1.97](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.96...v0.1.97) (2020-04-30)


### Bug Fixes

* services wizard pricing and linking fix ([48a449e](https://gitlab.com/appointy/waqt/ui/service2/commit/48a449e8eb51c728fcc182903da365cd2bdfc0e6))

### [0.1.96](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.95...v0.1.96) (2020-04-29)


### Bug Fixes

* service create ([568dcff](https://gitlab.com/appointy/waqt/ui/service2/commit/568dcffa6c56356b64bba5d5e46448ad172d16c9))

### [0.1.95](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.94...v0.1.95) (2020-04-29)


### Bug Fixes

* services add fix ([daa9a37](https://gitlab.com/appointy/waqt/ui/service2/commit/daa9a37ac66ccf5fdbcf0eaa33a4a0e25577a3a4))

### [0.1.94](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.93...v0.1.94) (2020-04-29)


### Bug Fixes

* services add fix ([f5970f9](https://gitlab.com/appointy/waqt/ui/service2/commit/f5970f9b4bd0edf0a15ade445adb1eb5b5badcdd))

### [0.1.93](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.92...v0.1.93) (2020-04-29)


### Bug Fixes

* disable previous in service ([a5ecf7e](https://gitlab.com/appointy/waqt/ui/service2/commit/a5ecf7e1ce7744141b06a3bda9f684256f89a966))

### [0.1.91](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.90...v0.1.91) (2020-04-27)


### Bug Fixes

* disable previous in service ([a5ecf7e](https://gitlab.com/appointy/waqt/ui/service2/commit/a5ecf7e1ce7744141b06a3bda9f684256f89a966))

### [0.1.92](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.91...v0.1.92) (2020-04-29)


### Bug Fixes

* services add fix ([81fea50](https://gitlab.com/appointy/waqt/ui/service2/commit/81fea502516ad4a51cef9a51fa6c805620cab459))

### [0.1.91](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.90...v0.1.91) (2020-04-29)


### Bug Fixes

* services add fix ([a841793](https://gitlab.com/appointy/waqt/ui/service2/commit/a841793d658cc7fedc06c2ce17259245e6c7d160))

### [0.1.90](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.89...v0.1.90) (2020-04-27)


### Bug Fixes

* fetch dependencies every time creating a service ([9078510](https://gitlab.com/appointy/waqt/ui/service2/commit/907851029e1de6480a50226c375759a001d27d1c))

### [0.1.89](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.88...v0.1.89) (2020-04-27)


### Bug Fixes

* category in service update ([e4972ac](https://gitlab.com/appointy/waqt/ui/service2/commit/e4972aceae47c6c260031b7cc92b6872fd9b0f6f))

### [0.1.88](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.87...v0.1.88) (2020-04-25)


### Bug Fixes

* margin in checkbox ([c647513](https://gitlab.com/appointy/waqt/ui/service2/commit/c6475132e432f1ca6dad49b3fae66424e957ccb1))
* open hours screen not visible ([d3ad927](https://gitlab.com/appointy/waqt/ui/service2/commit/d3ad927d34aa9e7f91b089a145c6da9f49592780))

### [0.1.87](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.86...v0.1.87) (2020-04-22)


### Features

* approval settings ([6a3b65d](https://gitlab.com/appointy/waqt/ui/service2/commit/6a3b65d483a05f4509e3b018c66112a196372b2e))

### [0.1.86](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.85...v0.1.86) (2020-04-22)


### Features

* customer booking rules ([49207fc](https://gitlab.com/appointy/waqt/ui/service2/commit/49207fcec61f6eca4a9c4ac027d9c4ef53b10e90))
* customer booking rules skeleton ([caf5636](https://gitlab.com/appointy/waqt/ui/service2/commit/caf5636f26ee262506d839451315d14b9453e325))
* customer booking rules with refetch ([1dc38f6](https://gitlab.com/appointy/waqt/ui/service2/commit/1dc38f697ffe749a31f5acb01d7eca7c30236d4b))


### Bug Fixes

* width ([477304e](https://gitlab.com/appointy/waqt/ui/service2/commit/477304e78b87519eb073091532d3fc85f74e52ff))

### [0.1.85](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.84...v0.1.85) (2020-04-21)


### Bug Fixes

* settings ux fix ([a37f123](https://gitlab.com/appointy/waqt/ui/service2/commit/a37f1234a9c0090a74bf7e88928ff831ac027a1c))

### [0.1.84](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.83...v0.1.84) (2020-04-18)


### Bug Fixes

* ux fix ([23d7f30](https://gitlab.com/appointy/waqt/ui/service2/commit/23d7f3043e661b6055befba6c2586b4237348f16))

### [0.1.83](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.82...v0.1.83) (2020-04-18)

### [0.1.82](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.81...v0.1.82) (2020-04-18)


### Bug Fixes

* pricing update in service ([df1b4ac](https://gitlab.com/appointy/waqt/ui/service2/commit/df1b4ac03be58696e88e3d2f9b4af6be684d97f4))

### [0.1.81](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.80...v0.1.81) (2020-04-17)


### Features

* aliasing ([fd9952f](https://gitlab.com/appointy/waqt/ui/service2/commit/fd9952faafb692525abd71d21524f2de30651773))
* handle staff pricing ([2ee5d4e](https://gitlab.com/appointy/waqt/ui/service2/commit/2ee5d4e3023d08a82fe4654a7ef3cbcff373de3d))
* handle staff pricing update ([5291410](https://gitlab.com/appointy/waqt/ui/service2/commit/52914105e61c7c6b51fe7380c34b98831da1bd9c))
* update staff pricing ([669c8c6](https://gitlab.com/appointy/waqt/ui/service2/commit/669c8c6b91171a56364f38cbfc0cf64dd5f6e7e0))
* update staff pricing call complete ([24abf88](https://gitlab.com/appointy/waqt/ui/service2/commit/24abf8843f1aff9441baa43ad3ae74b1cc721790))


### Bug Fixes

* refetch ([2db3177](https://gitlab.com/appointy/waqt/ui/service2/commit/2db31774ef58a567961a399cf99f5ade79742beb))
* update staff pricing mutation ([91b06bd](https://gitlab.com/appointy/waqt/ui/service2/commit/91b06bd4eb7b030600a1156abd7afd4e8c7a6ed4))

### [0.1.80](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.79...v0.1.80) (2020-04-16)


### Features

* alias hook ([ac43241](https://gitlab.com/appointy/waqt/ui/service2/commit/ac43241b34b73006cd65c63f39d907b9a143036a))

### [0.1.79](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.78...v0.1.79) (2020-04-14)


### Features

* color changer ([7cc1540](https://gitlab.com/appointy/waqt/ui/service2/commit/7cc1540100c0d511646636a47c11d35c0242645e))

### [0.1.78](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.75...v0.1.78) (2020-04-14)

### [0.1.76](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.73...v0.1.76) (2020-04-10)


### Features

* **bug fix:** import fix ([3d34780](https://gitlab.com/appointy/waqt/ui/service2/commit/3d34780a06a7f1f3be0ec7b449c9c80b46b456bc))
* **bug fix:** package.json ([bb7966a](https://gitlab.com/appointy/waqt/ui/service2/commit/bb7966ac04f16f4d22e15e345b3c882d276fc2df))
* **bug fix:** service link update ([f8e875d](https://gitlab.com/appointy/waqt/ui/service2/commit/f8e875de921fb0fec4c41de4d1df5aafe0896698))

### [0.1.76](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.75...v0.1.76) (2020-04-14)

### [0.1.76](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.73...v0.1.76) (2020-04-10)


### Features

* **bug fix:** import fix ([3d34780](https://gitlab.com/appointy/waqt/ui/service2/commit/3d34780a06a7f1f3be0ec7b449c9c80b46b456bc))
* **bug fix:** package.json ([bb7966a](https://gitlab.com/appointy/waqt/ui/service2/commit/bb7966ac04f16f4d22e15e345b3c882d276fc2df))
* **bug fix:** service link update ([f8e875d](https://gitlab.com/appointy/waqt/ui/service2/commit/f8e875de921fb0fec4c41de4d1df5aafe0896698))

### [0.1.75](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.74...v0.1.75) (2020-04-14)

### [0.1.74](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.73...v0.1.74) (2020-04-14)


### Bug Fixes

* step 4 and color in metadata ([8b8008f](https://gitlab.com/appointy/waqt/ui/service2/commit/8b8008fa644391ae7f87378c8ac2b609c2a00d2f))

### [0.1.73](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.72...v0.1.73) (2020-04-10)


### Bug Fixes

* link ([e51d15b](https://gitlab.com/appointy/waqt/ui/service2/commit/e51d15b032e3bd4de72b36a2cea614c341db9c7f))

### [0.1.72](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.71...v0.1.72) (2020-04-10)


### Bug Fixes

* link ([166a402](https://gitlab.com/appointy/waqt/ui/service2/commit/166a402b31549c9b90760e451a93d935ee95bcbc))

### [0.1.71](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.70...v0.1.71) (2020-04-10)


### Features

* show pricing in assign ([092e19b](https://gitlab.com/appointy/waqt/ui/service2/commit/092e19b5f5c37bebf489e2dd1437089b826f317a))
* show pricing in assign ([5e87337](https://gitlab.com/appointy/waqt/ui/service2/commit/5e87337ac36734b63c502646f8945e65d35a6487))

### [0.1.70](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.69...v0.1.70) (2020-04-10)


### Features

* open hours while adding ([aa9f083](https://gitlab.com/appointy/waqt/ui/service2/commit/aa9f0834d9e8f1d613014c546fcbea760fe0c5a1))


### Bug Fixes

* open hours while adding service ([dc4ce23](https://gitlab.com/appointy/waqt/ui/service2/commit/dc4ce237be553d48b18f515c252d7f5d5a59cc57))

### [0.1.69](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.68...v0.1.69) (2020-04-09)


### Features

* capacity in basic settings ([61a9314](https://gitlab.com/appointy/waqt/ui/service2/commit/61a931460816bdce86e2ca943b48ec0fadba72b9))

### [0.1.68](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.67...v0.1.68) (2020-04-09)


### Features

* pricing from service template ([ec92216](https://gitlab.com/appointy/waqt/ui/service2/commit/ec92216d1c58d3f2320ac91fbbf3a815e3c6fe21))


### Bug Fixes

* bug fix ([972c622](https://gitlab.com/appointy/waqt/ui/service2/commit/972c622d2f2d3ece982d247dc26afb0a29596587))

### [0.1.67](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.66...v0.1.67) (2020-04-08)

### [0.1.66](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.65...v0.1.66) (2020-04-08)

### [0.1.65](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.29...v0.1.65) (2020-04-08)


### Bug Fixes

* bug fix ([49867dd](https://gitlab.com/appointy/waqt/ui/service2/commit/49867dd003cabe14776f4124c1985ff5b71e2333))

### [0.1.64](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.63...v0.1.64) (2020-04-07)


### Bug Fixes

* roles in settings hooks ([7c89fb1](https://gitlab.com/appointy/waqt/ui/service2/commit/7c89fb1f08ef5e0ee7c934fbfc3d825e249a81ac))

### [0.1.63](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.62...v0.1.63) (2020-04-06)


### Features

* validation rules ([426d998](https://gitlab.com/appointy/waqt/ui/service2/commit/426d998cd97ad9a4b1b4ce5e5b67d3d9e18954d0))

### [0.1.62](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.61...v0.1.62) (2020-04-06)


### Bug Fixes

* assign and create ([72c113a](https://gitlab.com/appointy/waqt/ui/service2/commit/72c113a9027b276b47dd387d2e0dea2a0450bf71))
* auto complete ([4ed5dc2](https://gitlab.com/appointy/waqt/ui/service2/commit/4ed5dc2e2a706d8a3794f4bfc64a68b9712ed5ed))

### [0.1.61](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.60...v0.1.61) (2020-04-04)


### Features

* select service template ([57e48fd](https://gitlab.com/appointy/waqt/ui/service2/commit/57e48fd84d65fa6d8de08a0f1878f1303bdadb2c))

### [0.1.60](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.59...v0.1.60) (2020-04-04)


### Bug Fixes

* tax form ([cfe3b72](https://gitlab.com/appointy/waqt/ui/service2/commit/cfe3b72ea07239652fd4b5226dd69abca0c0adaf))

### [0.1.59](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.58...v0.1.59) (2020-04-02)


### Bug Fixes

* internal ([33bdec3](https://gitlab.com/appointy/waqt/ui/service2/commit/33bdec39882958847c1624109d1c8e765407171a))

### [0.1.58](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.57...v0.1.58) (2020-04-02)


### Bug Fixes

* service avatar ([6b18c36](https://gitlab.com/appointy/waqt/ui/service2/commit/6b18c36e2bde6e40128f5ad4778ed338c2087322))

### [0.1.57](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.56...v0.1.57) (2020-04-02)


### Bug Fixes

* service avatar ([625f013](https://gitlab.com/appointy/waqt/ui/service2/commit/625f013f3dd26e05a0d1cd036634610fab0bcdc7))

### [0.1.56](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.54...v0.1.56) (2020-04-02)


### Features

* **list:** added action for reordering ([2db5cfc](https://gitlab.com/appointy/waqt/ui/service2/commit/2db5cfcaed90c38c023e2d310f4ccf9418e6262e))


### Bug Fixes

* do not render settings if not in api ([8229e2e](https://gitlab.com/appointy/waqt/ui/service2/commit/8229e2e484ad739fce8f4b745fd2c9d463ca7565))

### [0.1.54](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.53...v0.1.54) (2020-04-02)


### Bug Fixes

* stop propagation ([19fdd75](https://gitlab.com/appointy/waqt/ui/service2/commit/19fdd756902c5a57263e2495c128392bc80c36d9))

### [0.1.53](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.52...v0.1.53) (2020-04-02)


### Bug Fixes

* location settings ([0cfaea3](https://gitlab.com/appointy/waqt/ui/service2/commit/0cfaea333121957ea30368b061273cf0186c0243))
* null settings handling ([e739f37](https://gitlab.com/appointy/waqt/ui/service2/commit/e739f37e98911e67c38616d59493b8584298e8c6))
* payment rules ([85b92b3](https://gitlab.com/appointy/waqt/ui/service2/commit/85b92b38ea1f81de522f34a3d17c932c4f526488))
* update service ([64ca753](https://gitlab.com/appointy/waqt/ui/service2/commit/64ca75315e9ac5b02bd9158407840738ceaa3865))
* update service ([c78cc88](https://gitlab.com/appointy/waqt/ui/service2/commit/c78cc888904785c73f3a154fa9ac705a1911bc07))

### [0.1.52](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.51...v0.1.52) (2020-04-01)

### [0.1.51](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.50...v0.1.51) (2020-04-01)


### Bug Fixes

* settings changes ([e53cfb8](https://gitlab.com/appointy/waqt/ui/service2/commit/e53cfb8e46470cb1e8bd88e3bec54e8772381d96))

### [0.1.50](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.49...v0.1.50) (2020-04-01)


### Features

* **list:** sortable categories ([5224d11](https://gitlab.com/appointy/waqt/ui/service2/commit/5224d1137b135adceaccbc4a678a256003bc18c7))


### Bug Fixes

* fadeout after success ([8a67b76](https://gitlab.com/appointy/waqt/ui/service2/commit/8a67b76962ed70e8b7029069a3591412eb27174e))
* minor ([1864988](https://gitlab.com/appointy/waqt/ui/service2/commit/18649889c715bd332b8d92238a82c6099721d37f))

### [0.1.49](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.48...v0.1.49) (2020-04-01)


### Bug Fixes

* schema changes ([a59389e](https://gitlab.com/appointy/waqt/ui/service2/commit/a59389e3aac8da851450c7d69995daaab2ae66aa))
* service settings ux ([0c8c7f3](https://gitlab.com/appointy/waqt/ui/service2/commit/0c8c7f37771f5d70e5d3d176e96321713298dae0))
* settings detail code ([be38b31](https://gitlab.com/appointy/waqt/ui/service2/commit/be38b3168cdb72b5f0783d7c491520995450af42))

### [0.1.48](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.47...v0.1.48) (2020-04-01)


### Bug Fixes

* add form ([d0df781](https://gitlab.com/appointy/waqt/ui/service2/commit/d0df7814ca528de9d4256cbf74db5fb61281438b))

### [0.1.47](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.46...v0.1.47) (2020-04-01)


### Bug Fixes

* step 3 css ([aee5c36](https://gitlab.com/appointy/waqt/ui/service2/commit/aee5c36b8416a848c83bff306abd82ee2530eb36))
* step 4 css ([a164f75](https://gitlab.com/appointy/waqt/ui/service2/commit/a164f753ae2a71dd23fa1d303f39a19c776ab2aa))

### [0.1.46](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.45...v0.1.46) (2020-04-01)


### Bug Fixes

* image upload ([b317ce6](https://gitlab.com/appointy/waqt/ui/service2/commit/b317ce623e563ec132a9450bbca4b88ae37006af))

### [0.1.45](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.44...v0.1.45) (2020-04-01)


### Bug Fixes

* step 1 with linking employee ([363ce7f](https://gitlab.com/appointy/waqt/ui/service2/commit/363ce7fc47b26be2cdf340415481438a06cfdb77))
* steps ([d62c6dd](https://gitlab.com/appointy/waqt/ui/service2/commit/d62c6dd020dd847f1c03919d568ed6b96f6dbe75))

### [0.1.44](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.42...v0.1.44) (2020-04-01)


### Features

* **list:** sortable list ([5003785](https://gitlab.com/appointy/waqt/ui/service2/commit/50037857320ca288cf23dd9621de3a9879cde7af))


### Bug Fixes

* **bug fix:** skip ([f5e9ae8](https://gitlab.com/appointy/waqt/ui/service2/commit/f5e9ae8175e3c1628e8d959159fb6b634f83ecc4))

### [0.1.42](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.41...v0.1.42) (2020-03-31)

### [0.1.41](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.40...v0.1.41) (2020-03-31)


### Bug Fixes

* null check ([138844f](https://gitlab.com/appointy/waqt/ui/service2/commit/138844fa5a641f396bf64034aee06cdc22ef3b58))
* **category:** skip ([e7ed82c](https://gitlab.com/appointy/waqt/ui/service2/commit/e7ed82c397b0aa6a1076d57526d1161cf9b963ea))

### [0.1.40](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.39...v0.1.40) (2020-03-31)


### Features

* step 4 ([05cb8a3](https://gitlab.com/appointy/waqt/ui/service2/commit/05cb8a3ba2fbb2659f49c8c2bfb7306714fa2525))

### [0.1.39](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.38...v0.1.39) (2020-03-31)


### Bug Fixes

* step 3 ([dcdc3ff](https://gitlab.com/appointy/waqt/ui/service2/commit/dcdc3ffbaa5418c4cf4ac8b1662e50544890ad03))
* **correction:** skip ([c07cb31](https://gitlab.com/appointy/waqt/ui/service2/commit/c07cb31a0cfb98f2ddc47b7ef1f6beb2de90ec4b))

### [0.1.38](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.37...v0.1.38) (2020-03-31)


### Bug Fixes

* material ui issue ([9f6941d](https://gitlab.com/appointy/waqt/ui/service2/commit/9f6941df172d956607eb8aace00796c7e997efcf))

### [0.1.37](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.36...v0.1.37) (2020-03-31)


### Features

* step 1 ([c1185ed](https://gitlab.com/appointy/waqt/ui/service2/commit/c1185ed054cdd625a8c02af58dd413276540d474))
* step 2 ([244b1ee](https://gitlab.com/appointy/waqt/ui/service2/commit/244b1ee92556ec9e5eb10461f75b4a6280af1163))


### Bug Fixes

* **fix:** skip ([01eb7ea](https://gitlab.com/appointy/waqt/ui/service2/commit/01eb7eac7fb3e32c1eddd860747589e8499dc43f))
* add form ([ea03332](https://gitlab.com/appointy/waqt/ui/service2/commit/ea033329c2c9501f77b8a19e4114b2a992bc840b))

### [0.1.36](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.35...v0.1.36) (2020-03-31)


### Features

* deactivate service ([104d961](https://gitlab.com/appointy/waqt/ui/service2/commit/104d9614a1c715b84ab31eeb52590fd70cb3f9d7))


### Bug Fixes

* duration and pricing in detail ([a20b2ae](https://gitlab.com/appointy/waqt/ui/service2/commit/a20b2ae95ab3bd7383854e5512d949ba64314d9f))

### [0.1.35](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.32...v0.1.35) (2020-03-31)


### Bug Fixes

* service ux ([358b218](https://gitlab.com/appointy/waqt/ui/service2/commit/358b218f45ad47066429459b3a00e65300bba01c))
* **correction:** skip ([f67efa0](https://gitlab.com/appointy/waqt/ui/service2/commit/f67efa07928615581a1f0f1ed0ffb3a443a861d7))
* **new ux fix:** skip ([40494ca](https://gitlab.com/appointy/waqt/ui/service2/commit/40494cae18796b3d1f8c9f5b127abc41df087fec))

### [0.1.32](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.31...v0.1.32) (2020-03-30)


### Bug Fixes

* settings in service ([905f327](https://gitlab.com/appointy/waqt/ui/service2/commit/905f327f77ccc9aba6f4048663a53d681cc0f588))
* **new ux:** skip ([13e6ccb](https://gitlab.com/appointy/waqt/ui/service2/commit/13e6ccbc4d229544c0092ce797161bdbc3da0493))
* app name in glance hook ([deb16b4](https://gitlab.com/appointy/waqt/ui/service2/commit/deb16b45643011d8a58c728c5b25ac323e6c9a3d))

### [0.1.31](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.30...v0.1.31) (2020-03-30)


### Bug Fixes

* employee count ([3f46102](https://gitlab.com/appointy/waqt/ui/service2/commit/3f46102c9a1de00e20b51dbdbd0ea124479bbfb2))

### [0.1.30](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.28...v0.1.30) (2020-03-30)


### Features

* employee service linking ([7ae225c](https://gitlab.com/appointy/waqt/ui/service2/commit/7ae225c013f5b0cda014ceb9dc019ce8ee79fde0))
* glance component ([b13d548](https://gitlab.com/appointy/waqt/ui/service2/commit/b13d5480f421a6328e987d74b1326ae20a0ecc2a))
* new detail ([0ce5fb3](https://gitlab.com/appointy/waqt/ui/service2/commit/0ce5fb3105079f628163b0a7365ff210dfbc8a64))


### Bug Fixes

* release issue ([404b3b4](https://gitlab.com/appointy/waqt/ui/service2/commit/404b3b4202cd134e82f8af99523c224aef88bd3a))

### [0.1.64](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.63...v0.1.64) (2020-04-07)


### Bug Fixes

* roles in settings hooks ([7c89fb1](https://gitlab.com/appointy/waqt/ui/service2/commit/7c89fb1f08ef5e0ee7c934fbfc3d825e249a81ac))

### [0.1.63](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.62...v0.1.63) (2020-04-06)


### Features

* validation rules ([426d998](https://gitlab.com/appointy/waqt/ui/service2/commit/426d998cd97ad9a4b1b4ce5e5b67d3d9e18954d0))

### [0.1.62](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.61...v0.1.62) (2020-04-06)


### Bug Fixes

* assign and create ([72c113a](https://gitlab.com/appointy/waqt/ui/service2/commit/72c113a9027b276b47dd387d2e0dea2a0450bf71))
* auto complete ([4ed5dc2](https://gitlab.com/appointy/waqt/ui/service2/commit/4ed5dc2e2a706d8a3794f4bfc64a68b9712ed5ed))

### [0.1.61](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.60...v0.1.61) (2020-04-04)


### Features

* select service template ([57e48fd](https://gitlab.com/appointy/waqt/ui/service2/commit/57e48fd84d65fa6d8de08a0f1878f1303bdadb2c))

### [0.1.60](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.59...v0.1.60) (2020-04-04)


### Bug Fixes

* tax form ([cfe3b72](https://gitlab.com/appointy/waqt/ui/service2/commit/cfe3b72ea07239652fd4b5226dd69abca0c0adaf))

### [0.1.59](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.58...v0.1.59) (2020-04-02)


### Bug Fixes

* internal ([33bdec3](https://gitlab.com/appointy/waqt/ui/service2/commit/33bdec39882958847c1624109d1c8e765407171a))

### [0.1.58](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.57...v0.1.58) (2020-04-02)


### Bug Fixes

* service avatar ([6b18c36](https://gitlab.com/appointy/waqt/ui/service2/commit/6b18c36e2bde6e40128f5ad4778ed338c2087322))

### [0.1.57](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.56...v0.1.57) (2020-04-02)


### Bug Fixes

* service avatar ([625f013](https://gitlab.com/appointy/waqt/ui/service2/commit/625f013f3dd26e05a0d1cd036634610fab0bcdc7))

### [0.1.56](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.54...v0.1.56) (2020-04-02)


### Features

* **list:** added action for reordering ([2db5cfc](https://gitlab.com/appointy/waqt/ui/service2/commit/2db5cfcaed90c38c023e2d310f4ccf9418e6262e))


### Bug Fixes

* do not render settings if not in api ([8229e2e](https://gitlab.com/appointy/waqt/ui/service2/commit/8229e2e484ad739fce8f4b745fd2c9d463ca7565))

### [0.1.55](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.54...v0.1.55) (2020-04-02)


### Features

* **list:** added action for reordering ([2db5cfc](https://gitlab.com/appointy/waqt/ui/service2/commit/2db5cfcaed90c38c023e2d310f4ccf9418e6262e))

### [0.1.54](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.53...v0.1.54) (2020-04-02)


### Bug Fixes

* stop propagation ([19fdd75](https://gitlab.com/appointy/waqt/ui/service2/commit/19fdd756902c5a57263e2495c128392bc80c36d9))

### [0.1.53](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.52...v0.1.53) (2020-04-02)


### Bug Fixes

* location settings ([0cfaea3](https://gitlab.com/appointy/waqt/ui/service2/commit/0cfaea333121957ea30368b061273cf0186c0243))
* null settings handling ([e739f37](https://gitlab.com/appointy/waqt/ui/service2/commit/e739f37e98911e67c38616d59493b8584298e8c6))
* payment rules ([85b92b3](https://gitlab.com/appointy/waqt/ui/service2/commit/85b92b38ea1f81de522f34a3d17c932c4f526488))
* update service ([64ca753](https://gitlab.com/appointy/waqt/ui/service2/commit/64ca75315e9ac5b02bd9158407840738ceaa3865))
* update service ([c78cc88](https://gitlab.com/appointy/waqt/ui/service2/commit/c78cc888904785c73f3a154fa9ac705a1911bc07))

### [0.1.52](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.51...v0.1.52) (2020-04-01)

### [0.1.51](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.49...v0.1.51) (2020-04-01)


### Features

* **list:** sortable categories ([5224d11](https://gitlab.com/appointy/waqt/ui/service2/commit/5224d1137b135adceaccbc4a678a256003bc18c7))


### Bug Fixes

* fadeout after success ([8a67b76](https://gitlab.com/appointy/waqt/ui/service2/commit/8a67b76962ed70e8b7029069a3591412eb27174e))
* minor ([1864988](https://gitlab.com/appointy/waqt/ui/service2/commit/18649889c715bd332b8d92238a82c6099721d37f))
* settings changes ([e53cfb8](https://gitlab.com/appointy/waqt/ui/service2/commit/e53cfb8e46470cb1e8bd88e3bec54e8772381d96))

### [0.1.50](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.49...v0.1.50) (2020-04-01)


### Features

* **list:** sortable categories ([5224d11](https://gitlab.com/appointy/waqt/ui/service2/commit/5224d1137b135adceaccbc4a678a256003bc18c7))

### [0.1.49](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.48...v0.1.49) (2020-04-01)


### Bug Fixes

* schema changes ([a59389e](https://gitlab.com/appointy/waqt/ui/service2/commit/a59389e3aac8da851450c7d69995daaab2ae66aa))
* service settings ux ([0c8c7f3](https://gitlab.com/appointy/waqt/ui/service2/commit/0c8c7f37771f5d70e5d3d176e96321713298dae0))
* settings detail code ([be38b31](https://gitlab.com/appointy/waqt/ui/service2/commit/be38b3168cdb72b5f0783d7c491520995450af42))

### [0.1.48](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.47...v0.1.48) (2020-04-01)


### Bug Fixes

* add form ([d0df781](https://gitlab.com/appointy/waqt/ui/service2/commit/d0df7814ca528de9d4256cbf74db5fb61281438b))

### [0.1.47](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.46...v0.1.47) (2020-04-01)


### Bug Fixes

* step 3 css ([aee5c36](https://gitlab.com/appointy/waqt/ui/service2/commit/aee5c36b8416a848c83bff306abd82ee2530eb36))
* step 4 css ([a164f75](https://gitlab.com/appointy/waqt/ui/service2/commit/a164f753ae2a71dd23fa1d303f39a19c776ab2aa))

### [0.1.46](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.45...v0.1.46) (2020-04-01)


### Bug Fixes

* image upload ([b317ce6](https://gitlab.com/appointy/waqt/ui/service2/commit/b317ce623e563ec132a9450bbca4b88ae37006af))

### [0.1.45](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.44...v0.1.45) (2020-04-01)


### Bug Fixes

* step 1 with linking employee ([363ce7f](https://gitlab.com/appointy/waqt/ui/service2/commit/363ce7fc47b26be2cdf340415481438a06cfdb77))
* steps ([d62c6dd](https://gitlab.com/appointy/waqt/ui/service2/commit/d62c6dd020dd847f1c03919d568ed6b96f6dbe75))

### [0.1.44](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.42...v0.1.44) (2020-04-01)


### Features

* **list:** sortable list ([5003785](https://gitlab.com/appointy/waqt/ui/service2/commit/50037857320ca288cf23dd9621de3a9879cde7af))


### Bug Fixes

* **bug fix:** skip ([f5e9ae8](https://gitlab.com/appointy/waqt/ui/service2/commit/f5e9ae8175e3c1628e8d959159fb6b634f83ecc4))

### [0.1.43](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.42...v0.1.43) (2020-03-31)


### Bug Fixes

* **bug fix:** skip ([f5e9ae8](https://gitlab.com/appointy/waqt/ui/service2/commit/f5e9ae8175e3c1628e8d959159fb6b634f83ecc4))

### [0.1.42](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.41...v0.1.42) (2020-03-31)


### Bug Fixes

* **category:** skip ([e7ed82c](https://gitlab.com/appointy/waqt/ui/service2/commit/e7ed82c397b0aa6a1076d57526d1161cf9b963ea))
* **correction:** skip ([c07cb31](https://gitlab.com/appointy/waqt/ui/service2/commit/c07cb31a0cfb98f2ddc47b7ef1f6beb2de90ec4b))
* **fix:** skip ([01eb7ea](https://gitlab.com/appointy/waqt/ui/service2/commit/01eb7eac7fb3e32c1eddd860747589e8499dc43f))

### [0.1.41](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.40...v0.1.41) (2020-03-31)


### Bug Fixes

* null check ([138844f](https://gitlab.com/appointy/waqt/ui/service2/commit/138844fa5a641f396bf64034aee06cdc22ef3b58))

### [0.1.40](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.39...v0.1.40) (2020-03-31)


### Features

* step 4 ([05cb8a3](https://gitlab.com/appointy/waqt/ui/service2/commit/05cb8a3ba2fbb2659f49c8c2bfb7306714fa2525))

### [0.1.39](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.38...v0.1.39) (2020-03-31)


### Bug Fixes

* step 3 ([dcdc3ff](https://gitlab.com/appointy/waqt/ui/service2/commit/dcdc3ffbaa5418c4cf4ac8b1662e50544890ad03))

### [0.1.38](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.37...v0.1.38) (2020-03-31)


### Bug Fixes

* material ui issue ([9f6941d](https://gitlab.com/appointy/waqt/ui/service2/commit/9f6941df172d956607eb8aace00796c7e997efcf))

### [0.1.37](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.36...v0.1.37) (2020-03-31)


### Features

* step 1 ([c1185ed](https://gitlab.com/appointy/waqt/ui/service2/commit/c1185ed054cdd625a8c02af58dd413276540d474))
* step 2 ([244b1ee](https://gitlab.com/appointy/waqt/ui/service2/commit/244b1ee92556ec9e5eb10461f75b4a6280af1163))


### Bug Fixes

* add form ([ea03332](https://gitlab.com/appointy/waqt/ui/service2/commit/ea033329c2c9501f77b8a19e4114b2a992bc840b))

### [0.1.36](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.35...v0.1.36) (2020-03-31)


### Features

* deactivate service ([104d961](https://gitlab.com/appointy/waqt/ui/service2/commit/104d9614a1c715b84ab31eeb52590fd70cb3f9d7))


### Bug Fixes

* duration and pricing in detail ([a20b2ae](https://gitlab.com/appointy/waqt/ui/service2/commit/a20b2ae95ab3bd7383854e5512d949ba64314d9f))

### [0.1.35](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.32...v0.1.35) (2020-03-31)


### Bug Fixes

* service ux ([358b218](https://gitlab.com/appointy/waqt/ui/service2/commit/358b218f45ad47066429459b3a00e65300bba01c))
* **correction:** skip ([f67efa0](https://gitlab.com/appointy/waqt/ui/service2/commit/f67efa07928615581a1f0f1ed0ffb3a443a861d7))
* **new ux fix:** skip ([40494ca](https://gitlab.com/appointy/waqt/ui/service2/commit/40494cae18796b3d1f8c9f5b127abc41df087fec))

### [0.1.34](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.33...v0.1.34) (2020-03-30)


### Bug Fixes

* **correction:** skip ([f67efa0](https://gitlab.com/appointy/waqt/ui/service2/commit/f67efa07928615581a1f0f1ed0ffb3a443a861d7))

### [0.1.33](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.32...v0.1.33) (2020-03-30)


### Bug Fixes

* **new ux fix:** skip ([40494ca](https://gitlab.com/appointy/waqt/ui/service2/commit/40494cae18796b3d1f8c9f5b127abc41df087fec))

### [0.1.32](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.31...v0.1.32) (2020-03-30)


### Bug Fixes

* settings in service ([905f327](https://gitlab.com/appointy/waqt/ui/service2/commit/905f327f77ccc9aba6f4048663a53d681cc0f588))
* **new ux:** skip ([13e6ccb](https://gitlab.com/appointy/waqt/ui/service2/commit/13e6ccbc4d229544c0092ce797161bdbc3da0493))
* app name in glance hook ([deb16b4](https://gitlab.com/appointy/waqt/ui/service2/commit/deb16b45643011d8a58c728c5b25ac323e6c9a3d))

### [0.1.31](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.30...v0.1.31) (2020-03-30)


### Bug Fixes

* employee count ([3f46102](https://gitlab.com/appointy/waqt/ui/service2/commit/3f46102c9a1de00e20b51dbdbd0ea124479bbfb2))

### [0.1.30](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.29...v0.1.30) (2020-03-30)


### Bug Fixes

* release issue ([404b3b4](https://gitlab.com/appointy/waqt/ui/service2/commit/404b3b4202cd134e82f8af99523c224aef88bd3a))

### [0.1.29](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.28...v0.1.29) (2020-03-30)


### Features

* employee service linking ([7ae225c](https://gitlab.com/appointy/waqt/ui/service2/commit/7ae225c013f5b0cda014ceb9dc019ce8ee79fde0))
* glance component ([b13d548](https://gitlab.com/appointy/waqt/ui/service2/commit/b13d5480f421a6328e987d74b1326ae20a0ecc2a))
* new detail ([0ce5fb3](https://gitlab.com/appointy/waqt/ui/service2/commit/0ce5fb3105079f628163b0a7365ff210dfbc8a64))


### Bug Fixes

* bug fix ([4403433](https://gitlab.com/appointy/waqt/ui/service2/commit/44034335a936aa7840a35d1dcd4f543d59f078f5))

### [0.1.28](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.27...v0.1.28) (2020-03-29)

### [0.1.27](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.26...v0.1.27) (2020-03-29)

### [0.1.26](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.24...v0.1.26) (2020-03-29)


### Bug Fixes

* bug fix ([3b16481](https://gitlab.com/appointy/waqt/ui/service2/commit/3b164816b711048cef433ee1caf28a5855acd7f0))
* **route:** skip ([831708d](https://gitlab.com/appointy/waqt/ui/service2/commit/831708d9457558224898c191adabd74649944d78))

### [0.1.25](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.24...v0.1.25) (2020-03-26)


### Bug Fixes

* **route:** skip ([831708d](https://gitlab.com/appointy/waqt/ui/service2/commit/831708d9457558224898c191adabd74649944d78))

### [0.1.24](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.23...v0.1.24) (2020-03-25)


### Bug Fixes

* **bug fix:** bugFix ([ffb9c71](https://gitlab.com/appointy/waqt/ui/service2/commit/ffb9c7120e645623353b32e349852662a367b3e3))
* **correction:** correction ([bff5795](https://gitlab.com/appointy/waqt/ui/service2/commit/bff5795d5dd3430141a0f477fddc7a7186e59ee5))
* **newux:** new ux ([207d0ec](https://gitlab.com/appointy/waqt/ui/service2/commit/207d0ecb402a89cd48e35aed5ee3496b00bdea83))

### [0.1.23](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.22...v0.1.23) (2020-03-24)


### Bug Fixes

* **update:** update packages ([8acde42](https://gitlab.com/appointy/waqt/ui/service2/commit/8acde42e28fc2b0d32093037a2151e6e3cba3d14))

### [0.1.22](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.21...v0.1.22) (2020-03-23)


### Bug Fixes

* **gallery:** gallery mutation ([7801808](https://gitlab.com/appointy/waqt/ui/service2/commit/780180889b08d0723722846c616bf0f89e77082d))

### [0.1.21](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.20...v0.1.21) (2020-03-22)


### Bug Fixes

* **linkinh:** linking ([774807c](https://gitlab.com/appointy/waqt/ui/service2/commit/774807c9413302ce7b3af817db24244c2ee55f5d))

### [0.1.20](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.19...v0.1.20) (2020-03-22)


### Bug Fixes

* **package:** package upgrade ([4dda010](https://gitlab.com/appointy/waqt/ui/service2/commit/4dda01055e35328a3499e98a2b563308c8c0d410))

### [0.1.19](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.18...v0.1.19) (2020-03-22)


### Bug Fixes

* **linking:** linking ([d7fda1f](https://gitlab.com/appointy/waqt/ui/service2/commit/d7fda1f765f354d2e0a4a1538370310a58a5f98c))

### [0.1.18](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.17...v0.1.18) (2020-03-21)


### Bug Fixes

* **gallery:** gallery ([9e56e37](https://gitlab.com/appointy/waqt/ui/service2/commit/9e56e37e9e2c520d274a0786afae6ffe1f131452))

### [0.1.17](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.16...v0.1.17) (2020-03-21)


### Bug Fixes

* **linking:** linking ([46f0b58](https://gitlab.com/appointy/waqt/ui/service2/commit/46f0b583d38945569967cd3336364d7a67016195))

### [0.1.16](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.14...v0.1.16) (2020-02-28)


### Bug Fixes

* **linking:** linking ([4643a22](https://gitlab.com/appointy/waqt/ui/service2/commit/4643a225b6cd38eb1bd37c1e75c65cd07c60651c))

### [0.1.15](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.14...v0.1.15) (2020-02-27)

### [0.1.14](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.13...v0.1.14) (2020-02-25)


### Bug Fixes

* settings hook path ([d3dd3b1](https://gitlab.com/appointy/waqt/ui/service2/commit/d3dd3b1250c5f2b2072061553f27a88c525b42fe))

### [0.1.13](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.12...v0.1.13) (2020-02-25)


### Features

* assign employees to service ([4226732](https://gitlab.com/appointy/waqt/ui/service2/commit/42267321c5c681d9e5d3662eef4232887a713e61))
* assign services to employee ([6f34ed6](https://gitlab.com/appointy/waqt/ui/service2/commit/6f34ed6c49c65f5498720512cde231667185bfde))

### [0.1.12](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.11...v0.1.12) (2020-02-25)


### Bug Fixes

* delete ([2f61218](https://gitlab.com/appointy/waqt/ui/service2/commit/2f61218771625f68956abe69e03b2b56a6f8bc67))

### [0.1.11](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.4...v0.1.11) (2020-02-25)


### Bug Fixes

* version ([f2ce9d6](https://gitlab.com/appointy/waqt/ui/service2/commit/f2ce9d6f916abe6b55b0267d516e55cf0d56d4f8))

### [0.1.4](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.3...v0.1.4) (2020-02-25)


### Bug Fixes

* import react-virtualized-auto-sizer ([523713f](https://gitlab.com/appointy/waqt/ui/service2/commit/523713f1223bd949d017968b97a0a0cc319a18e6))

### [0.1.3](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.2...v0.1.3) (2020-02-25)

### [0.1.2](https://gitlab.com/appointy/waqt/ui/service2/compare/v0.1.1...v0.1.2) (2020-02-25)


### Bug Fixes

* index.tsx ([77a9bd2](https://gitlab.com/appointy/waqt/ui/service2/commit/77a9bd2144b42dc571be4268f03ed893f6f7c094))

### 0.1.1 (2020-02-25)


### Features

* initial porting complete ([d17807e](https://gitlab.com/appointy/waqt/ui/service2/commit/d17807e984da0d37ef07b6184e08eb8b55f85fad))
