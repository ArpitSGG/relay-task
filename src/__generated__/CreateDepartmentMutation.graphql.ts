/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type CreateDepartmentInput = {
    clientMutationId?: string | null;
    department?: DepartmentInput | null;
    parent?: string | null;
};
export type DepartmentInput = {
    description?: string | null;
    id?: string | null;
    metadata?: string | null;
    name?: string | null;
};
export type CreateDepartmentMutationVariables = {
    input?: CreateDepartmentInput | null;
};
export type CreateDepartmentMutationResponse = {
    readonly createDepartment: {
        readonly clientMutationId: string;
        readonly payload: {
            readonly id: string;
            readonly name: string;
            readonly description: string;
            readonly metadata: string | null;
        };
    };
};
export type CreateDepartmentMutation = {
    readonly response: CreateDepartmentMutationResponse;
    readonly variables: CreateDepartmentMutationVariables;
};



/*
mutation CreateDepartmentMutation(
  $input: CreateDepartmentInput
) {
  createDepartment(input: $input) {
    clientMutationId
    payload {
      id
      name
      description
      metadata
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "CreateDepartmentPayload",
    "kind": "LinkedField",
    "name": "createDepartment",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "clientMutationId",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Department",
        "kind": "LinkedField",
        "name": "payload",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "description",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "metadata",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "CreateDepartmentMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "CreateDepartmentMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "59998fff78acba1c61cf5053bc57edf3",
    "id": null,
    "metadata": {},
    "name": "CreateDepartmentMutation",
    "operationKind": "mutation",
    "text": "mutation CreateDepartmentMutation(\n  $input: CreateDepartmentInput\n) {\n  createDepartment(input: $input) {\n    clientMutationId\n    payload {\n      id\n      name\n      description\n      metadata\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = 'e2e3ed13cf572c92d89e6fb000e9da11';
export default node;
