/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type DepartmentUpdate_departments = ReadonlyArray<{
    readonly id: string;
    readonly name: string;
    readonly description: string;
    readonly metadata: string | null;
    readonly " $refType": "DepartmentUpdate_departments";
}>;
export type DepartmentUpdate_departments$data = DepartmentUpdate_departments;
export type DepartmentUpdate_departments$key = ReadonlyArray<{
    readonly " $data"?: DepartmentUpdate_departments$data;
    readonly " $fragmentRefs": FragmentRefs<"DepartmentUpdate_departments">;
}>;



const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": {
    "plural": true
  },
  "name": "DepartmentUpdate_departments",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "metadata",
      "storageKey": null
    }
  ],
  "type": "Department",
  "abstractKey": null
};
(node as any).hash = '2e74dd97012ce98cd2fb92c87df0bd4d';
export default node;
