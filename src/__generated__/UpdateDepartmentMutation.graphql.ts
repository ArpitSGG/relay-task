/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type UpdateDepartmentInput = {
    clientMutationId?: string | null;
    department?: DepartmentInput | null;
    updateMask?: FieldMaskInput | null;
};
export type DepartmentInput = {
    description?: string | null;
    id?: string | null;
    metadata?: string | null;
    name?: string | null;
};
export type FieldMaskInput = {
    paths?: Array<string | null> | null;
};
export type UpdateDepartmentMutationVariables = {
    input?: UpdateDepartmentInput | null;
};
export type UpdateDepartmentMutationResponse = {
    readonly updateDepartment: {
        readonly payload: {
            readonly id: string;
            readonly description: string;
            readonly metadata: string | null;
            readonly name: string;
        };
    };
};
export type UpdateDepartmentMutation = {
    readonly response: UpdateDepartmentMutationResponse;
    readonly variables: UpdateDepartmentMutationVariables;
};



/*
mutation UpdateDepartmentMutation(
  $input: UpdateDepartmentInput
) {
  updateDepartment(input: $input) {
    payload {
      id
      description
      metadata
      name
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "UpdateDepartmentPayload",
    "kind": "LinkedField",
    "name": "updateDepartment",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Department",
        "kind": "LinkedField",
        "name": "payload",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "description",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "metadata",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "UpdateDepartmentMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "UpdateDepartmentMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "e81fdfdfcdd297a7d75a3610f533db89",
    "id": null,
    "metadata": {},
    "name": "UpdateDepartmentMutation",
    "operationKind": "mutation",
    "text": "mutation UpdateDepartmentMutation(\n  $input: UpdateDepartmentInput\n) {\n  updateDepartment(input: $input) {\n    payload {\n      id\n      description\n      metadata\n      name\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = '0a95fe45fd935b13b426567b056a6b25';
export default node;
