import { Trans } from '@lingui/macro'
import { useAlert } from '@saastack/core'
import { FormContainer } from '@saastack/layouts/containers'
import { FormContainerProps } from '@saastack/layouts/types'
import React, { useState } from 'react'
import { Variables } from 'react-relay'
import { useRelayEnvironment } from 'react-relay/hooks'
import DepartmentAddFormComponent from '../forms/DepartmentAddForm'
import CreateDepartmentMutation from '../mutations/CreateDepartmentMutation'
import DepartmentAddInitialValues from '../utils/DepartmentAddInitialValues'
import DepartmentAddValidations from '../utils/DepartmentAddValidations'
import { DepartmentInput } from '../__generated__/CreateDepartmentMutation.graphql'

interface Props extends Omit<FormContainerProps, 'formId'> {
    variables: Variables
    onClose: () => void
    onSuccess: () => void
}

const formId = 'department-add-form'

const DepartmentAdd: React.FC<Props> = ({ roles, variables, onClose, onSuccess, ...props }) => {
    const environment = useRelayEnvironment()
    const showAlert = useAlert()
    const [loading, setLoading] = useState(false)

    const handleSubmit = (values: DepartmentInput) => {
        setLoading(true)
        const department = {
            ...values,
        }
        CreateDepartmentMutation.commit(environment, variables, department, {
            onSuccess: handleSuccess,
            onError,
        })
    }

    const onError = (e: string) => {
        setLoading(false)
        showAlert(e, {
            variant: 'error',
        })
    }

    const handleSuccess = (response: DepartmentInput) => {
        setLoading(false)
        showAlert(<Trans>Department added successfully!</Trans>, {
            variant: 'info',
        })
        onSuccess()
    }

    const initialValues = {
        ...DepartmentAddInitialValues,
    }

    return (
        <FormContainer
            open
            onClose={onClose}
            header={<Trans>New Department</Trans>}
            formId={formId}
            loading={loading}
            {...props}
        >
            <DepartmentAddFormComponent
                onSubmit={handleSubmit}
                id={formId}
                initialValues={initialValues}
                validationSchema={DepartmentAddValidations}
            />
        </FormContainer>
    )
}

export default DepartmentAdd
