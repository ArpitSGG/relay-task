import { Trans } from '@lingui/macro'
import {
    IconButton,
    List,
    ListItem,
    ListItemAvatar,
    ListItemSecondaryAction,
    ListItemText,
    Tooltip,
} from '@material-ui/core'

import { Avatar } from '@saastack/components'
import { DeleteOutlined } from '@material-ui/icons'
import { makeStyles } from '@material-ui/styles'
import React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { DepartmentList_departments } from '../__generated__/DepartmentList_departments.graphql'

interface Props {
    departments: DepartmentList_departments
    onClick: (id: string, action: 'UPDATE' | 'DELETE') => void
}

const useStyles = makeStyles({
    list: {
        '& .MuiListItemSecondaryAction-root': {
            visibility: 'hidden',
        },
        '& li:hover': {
            '& .MuiListItemSecondaryAction-root': {
                visibility: 'visible',
            },
        },
    },
})

const DepartmentList: React.FC<Props> = ({ departments, onClick }) => {
    const classes = useStyles()
    return (
        <List className={classes.list}>
            {departments.map((department) => (
                <ListItem
                    key={department.id}
                    divider
                    button
                    onClick={() => onClick(department.id, 'UPDATE')}
                >
                    <ListItemAvatar>
                        <Avatar title={department.name[0]} />
                    </ListItemAvatar>
                    <ListItemText primary={department.name} secondary={department.description} />
                    <ListItemSecondaryAction>
                        <Tooltip title={<Trans>Delete {department.name}</Trans>}>
                            <IconButton onClick={() => onClick(department.id, 'DELETE')}>
                                <DeleteOutlined />
                            </IconButton>
                        </Tooltip>
                    </ListItemSecondaryAction>
                </ListItem>
            ))}
        </List>
    )
}

export default createFragmentContainer(DepartmentList, {
    departments: graphql`
        fragment DepartmentList_departments on Department @relay(plural: true) {
            id
            name
            description
        }
    `,
})
