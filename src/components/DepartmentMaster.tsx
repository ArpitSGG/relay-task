import { Trans } from '@lingui/macro'
import { AddOutlined } from '@material-ui/icons'
import { ActionItem } from '@saastack/components/Actions'
import { Layout } from '@saastack/layouts'
import React, { useState } from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { DepartmentMaster_departments } from '../__generated__/DepartmentMaster_departments.graphql'
import DepartmentAdd from './DepartmentAdd'
import DepartmentDelete from './DepartmentDelete'
import DepartmentList from './DepartmentList'
import DepartmentUpdate from './DepartmentUpdate'

interface Props {
    departments: DepartmentMaster_departments
    parent: string
    refetch: () => void
}
const DepartmentMaster: React.FC<Props> = ({
    departments: {
        departments: { department: departments },
    },
    parent,
    refetch,
}: Props) => {
    const [openAdd, setOpenAdd] = useState(false)
    const [openUpdate, setOpenUpdate] = useState(false)
    const [openDelete, setOpenDelete] = useState(false)
    const [selected, setSelected] = useState('')
    const variables = { parent }

    const handleClick = (id: string, action: 'UPDATE' | 'DELETE') => {
        setSelected(id)
        if (action === 'UPDATE') {
            setOpenUpdate(true)
        } else {
            setOpenDelete(true)
        }
    }

    const actions: ActionItem[] = [
        {
            icon: AddOutlined,
            title: <Trans>Add</Trans>,
            onClick: () => setOpenAdd(true),
        },
    ]

    return (
        <Layout
            header={<Trans>Departments</Trans>}
            col1={<DepartmentList departments={departments} onClick={handleClick} />}
            actions={actions}
        >
            {openAdd && (
                <DepartmentAdd
                    variables={variables}
                    onClose={() => setOpenAdd(false)}
                    onSuccess={() => {
                        setOpenAdd(false)
                        refetch()
                    }}
                />
            )}

            {openDelete && (
                <DepartmentDelete
                    id={selected}
                    onClose={() => {
                        setOpenDelete(false)
                        setSelected('')
                    }}
                    onSuccess={() => {
                        setOpenDelete(false)
                        refetch()
                    }}
                    variables={variables}
                />
            )}

            {openUpdate && (
                <DepartmentUpdate
                    id={selected}
                    departments={departments}
                    onClose={() => {
                        setOpenUpdate(false)
                        setSelected('')
                    }}
                    onSuccess={() => {
                        setOpenUpdate(false)
                        refetch()
                    }}
                />
            )}
        </Layout>
    )
}

export default createFragmentContainer(DepartmentMaster, {
    departments: graphql`
        fragment DepartmentMaster_departments on Query
        @argumentDefinitions(parent: { type: "String" }) {
            departments(parent: $parent) {
                department {
                    id
                    name
                    ...DepartmentList_departments
                    ...DepartmentUpdate_departments
                }
            }
        }
    `,
})
