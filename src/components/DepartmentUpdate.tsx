import React from 'react'
import { FormContainer } from '@saastack/layouts/containers'
import { FormContainerProps } from '@saastack/layouts'
import { createFragmentContainer, graphql } from 'react-relay'
import { useRelayEnvironment } from 'react-relay/hooks'
import { Mutable, useAlert } from '@saastack/core'
import { useState } from 'react'
import { useEffect } from 'react'
import { DepartmentInput } from '../__generated__/CreateDepartmentMutation.graphql'
import UpdateDepartmentMutation from '../mutations/UpdateDepartmentMutation'
import { DepartmentUpdate_departments } from '../__generated__/DepartmentUpdate_departments.graphql'
import { Trans } from '@lingui/macro'
import DepartmentAddForm from '../forms/DepartmentAddForm'
import DepartmentAddValidations from '../utils/DepartmentAddValidations'

interface Props extends Omit<FormContainerProps, 'formId'> {
    id: string
    departments: DepartmentUpdate_departments
    onClose: () => void
    onSuccess: () => void
}

const formId = 'department-update-form'

const DepartmentUpdate: React.FC<Props> = ({ id, departments, onClose, onSuccess, ...props }) => {
    const environment = useRelayEnvironment()
    const showAlert = useAlert()
    const [loading, setLoading] = useState(false)

    const department = departments.find((d) => d.id === id)

    useEffect(() => {
        !department && onClose()
    }, [department])

    if (!department) return null

    const handleSubmit = (department: DepartmentInput) => {
        setLoading(true)
        UpdateDepartmentMutation.commit(environment, department, ['name', 'description'], {
            onSuccess: handleSuccess,
            onError: handleError,
        })
    }

    const handleSuccess = () => {
        setLoading(false)
        showAlert(<Trans>Department updated successfully!</Trans>, {
            variant: 'info',
        })
        onSuccess()
    }

    const handleError = (e: string) => {
        setLoading(false)
        showAlert(e, {
            variant: 'error',
        })
    }
    const initialValues = department as Mutable<DepartmentUpdate_departments[0]>

    return (
        <FormContainer
            onClose={onClose}
            header={<Trans>Update Department</Trans>}
            formId={formId}
            loading={loading}
            open
            {...props}
        >
            <DepartmentAddForm
                update
                onSubmit={handleSubmit}
                id={formId}
                initialValues={initialValues}
                validationSchema={DepartmentAddValidations}
            />
        </FormContainer>
    )
}

export default createFragmentContainer(DepartmentUpdate, {
    departments: graphql`
        fragment DepartmentUpdate_departments on Department @relay(plural: true) {
            id
            name
            description
            metadata
        }
    `,
})
