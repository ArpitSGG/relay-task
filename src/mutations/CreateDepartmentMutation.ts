import { MutationCallbacks } from '@saastack/relay'
import { commitMutation, graphql, Variables } from 'react-relay'
import { Disposable, Environment } from 'relay-runtime'
import {
    CreateDepartmentInput,
    CreateDepartmentMutation,
    CreateDepartmentMutationResponse,
    DepartmentInput,
} from '../__generated__/CreateDepartmentMutation.graphql'

const mutation = graphql`
    mutation CreateDepartmentMutation($input: CreateDepartmentInput) {
        createDepartment(input: $input) {
            clientMutationId
            payload {
                id
                name
                description
                metadata
            }
        }
    }
`

let tempID = 0

const commit = (
    environment: Environment,
    variables: Variables,
    department: DepartmentInput,
    callbacks?: MutationCallbacks<DepartmentInput>
): Disposable => {
    const input: CreateDepartmentInput = {
        parent: variables.parent,
        department: {
            ...department,
            description: window.btoa(department.description || ''),
        },
        clientMutationId: `${tempID++}`,
    }

    return commitMutation<CreateDepartmentMutation>(environment, {
        mutation,
        variables: {
            input,
        },
        onError: (error: Error) => {
            if (callbacks?.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: (response: CreateDepartmentMutationResponse) => {
            if (callbacks?.onSuccess) {
                callbacks.onSuccess({ ...department, ...response.createDepartment.payload })
            }
        },
    })
}

export default { commit }
