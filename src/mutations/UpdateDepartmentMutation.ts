import { MutationCallbacks } from '@saastack/relay'
import { commitMutation, Environment, graphql } from 'relay-runtime'
import { DepartmentInput } from '../__generated__/CreateDepartmentMutation.graphql'
import {
    UpdateDepartmentInput,
    UpdateDepartmentMutation,
    UpdateDepartmentMutationResponse,
} from '../__generated__/UpdateDepartmentMutation.graphql'

const mutation = graphql`
    mutation UpdateDepartmentMutation($input: UpdateDepartmentInput) {
        updateDepartment(input: $input) {
            payload {
                id
                description
                metadata
                name
            }
        }
    }
`

let tempID = 0

const commit = (
    environment: Environment,
    department: DepartmentInput,
    updateMask: Array<keyof DepartmentInput>,
    callbacks?: MutationCallbacks<DepartmentInput>
) => {
    const input: UpdateDepartmentInput = {
        clientMutationId: `${tempID++}`,
        department: {
            ...department,
            description: window.btoa(department.description || ''),
        },
        updateMask: { paths: updateMask },
    }
    return commitMutation<UpdateDepartmentMutation>(environment, {
        mutation,
        variables: {
            input,
        },
        onError: (error) => {
            if (callbacks?.onError) {
                const message = error.message.split(`\n`)[1]
                callbacks.onError(message)
            }
        },
        onCompleted: (response: UpdateDepartmentMutationResponse) => {
            if (callbacks?.onSuccess) {
                callbacks.onSuccess({ ...department, ...response.updateDepartment.payload })
            }
        },
    })
}

export default { commit }
