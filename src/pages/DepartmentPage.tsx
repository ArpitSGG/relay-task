import { useConfig } from '@saastack/core'
import React, { useState, useRef, useEffect } from 'react'
import { useRelayEnvironment } from 'react-relay/hooks'
import { graphql } from 'relay-runtime'

import { ErrorComponent, Loading } from '@saastack/components'
import {
    DepartmentPageQuery,
    DepartmentPageQueryResponse,
    DepartmentPageQueryVariables,
} from '../__generated__/DepartmentPageQuery.graphql'
import { fetchQuery } from '@saastack/relay'
import DepartmentMaster from '../components/DepartmentMaster'

interface Props {}

const query = graphql`
    query DepartmentPageQuery($parent: String) {
        ...DepartmentMaster_departments @arguments(parent: $parent)
    }
`

const DepartmentPage = (props: Props) => {
    const { companyId } = useConfig()
    const [loading, setLoading] = useState(true)
    const environment = useRelayEnvironment()
    const [error, setError] = useState<any>(null)
    const dataRef = useRef<DepartmentPageQueryResponse | null>(null)

    const fetchDepartments = () => {
        const variables: DepartmentPageQueryVariables = {
            parent: companyId,
        }
        fetchQuery<DepartmentPageQuery>(environment, query, variables, { force: true })
            .then((resp) => {
                dataRef.current = resp
                setLoading(false)
            })
            .catch((error) => {
                setError(error)
                setLoading(false)
            })
    }
    useEffect(fetchDepartments, [])
    if (loading) {
        return <Loading />
    }
    if (error) {
        return <ErrorComponent error={error} />
    }

    return (
        <DepartmentMaster
            parent={companyId!}
            departments={dataRef.current!}
            refetch={fetchDepartments}
        />
    )
}

export default DepartmentPage
