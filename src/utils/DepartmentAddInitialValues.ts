import { DepartmentInput } from '../__generated__/CreateDepartmentMutation.graphql'

const DepartmentAddInitialValues: DepartmentInput = {
    id: '',
    name: '',
    description: '',
    roleIds: [],
}

export default DepartmentAddInitialValues
